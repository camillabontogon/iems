module LeavesHelper

  def select_leave_type
    ['Sick Leave','Sick Leave w/o Pay','Vacation Leave', 'Emergency Leave', 'Maternity/Paternity Leave', 'Birthday Leave']
  end

end
