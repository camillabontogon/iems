class EmployeesController < ApplicationController
layout :set_layout

skip_before_action :verify_authenticity_token, :only => [:create, :update, :add_sick_leave_credit, :add_vacation_leave_credit]
before_filter :check_session
before_filter :admin_only, :except => [:update, :show]

def index
  @employees = Employee.where(status: 'A').all.paginate(:page => params[:page], :per_page => 10)
  respond_to do |format|
    format.html
    format.csv { send_data @employees.to_csv }
  end
end

def search
  if params.has_key?(:search)
    @employees = Employee.where(status: 'A').where('first_name LIKE ? OR last_name LIKE ? ', '%'+params[:search]+'%','%'+params[:search]+'%').all.paginate(:page => params[:page], :per_page => 10)
  else
    redirect_to employees_path
  end
end

def add_sick_leave_credit
  @employee = Employee.find(params[:employee_id])
  @employee.sick_leave_credit = @employee.sick_leave_credit.to_f + params[:value].to_f
  @employee.sick_leave_balance = @employee.sick_leave_balance.to_f + params[:value].to_f

  if @employee.save
    respond_to do |format|
      format.json {render json: ["credit"=>@employee.sick_leave_credit, "balance"=>@employee.sick_leave_balance], status: :created }
    end
  else
    respond_to do |format|
      format.json {render json: ["1"=>"Error"], status: :unprocessable_entity }
    end
  end
  

end

def add_vacation_leave_credit
  @employee = Employee.find(params[:employee_id])
  @employee.vacation_leave_credit = @employee.vacation_leave_credit.to_f + params[:value].to_f
  @employee.vacation_leave_balance = @employee.vacation_leave_balance.to_f + params[:value].to_f

  if @employee.save
    respond_to do |format|
      format.json {render json: ["credit"=>@employee.vacation_leave_credit, "balance"=>@employee.vacation_leave_balance], status: :created }
    end
  else
    respond_to do |format|
      format.json {render json: ["1"=>"Error"], status: :unprocessable_entity }
    end
  end
  

end

def show

  if is_admin || current_user.role == "Manager"
    @employee = Employee.find(params[:id])
    @leaves = Leave.where(employee_id: params[:id]).where(status: 'A').all

    vl_count = 0
    sl_count = 0

    if @leaves.blank? == false
    @leaves.each do |l|
      if l.leave_type == 'Vacation Leave'
        vl_count = vl_count + l.no_of_days
      elsif l.leave_type == 'Sick Leave'
        sl_count = sl_count + l.no_of_days
      end
    end

    @vl = vl_count
    @sl = sl_count
    end

  else
    @employee = Employee.find(session[:user_id])
    @leaves = Leave.where(employee_id: session[:user_id]).where(status: 'A').all

    vl_count = 0
    sl_count = 0

    if @leaves.blank? == false
    @leaves.each do |l|
      if l.leave_type == 'Vacation Leave'
        vl_count = vl_count + l.no_of_days
      elsif l.leave_type == 'Sick Leave'
        sl_count = sl_count + l.no_of_days
      end
    end

    @vl = vl_count
    @sl = sl_count
    end

  end

end

def new
  @employee = Employee.new
end

def create
  @employee = Employee.new(params[:employee])
  @employee.time_in = '08:30:00'
  @employee.time_out = '18:30:00'
  @employee.sick_leave_balance = 0
  @employee.vacation_leave_balance = 0
  @employee.status = 'A'
  if @employee.save
    redirect_to employees_path#, :notice => "job_level saved"
  else
    render "new"
  end
end

def edit
  @employee = Employee.find(params[:id])
  @leaves = Leave.where(employee_id: params[:id]).where(status: 'A').all

  vl_count = 0
  sl_count = 0

  if @leaves.blank? == false
  @leaves.each do |l|
    if l.leave_type == 'Vacation Leave'
      vl_count = vl_count + l.no_of_days
    elsif l.leave_type == 'Sick Leave'
      sl_count = sl_count + l.no_of_days
    end
  end

  @vl = vl_count
  @sl = sl_count
  end
end

def update

  #only an admin can edit other employee details. 
  @employee = is_admin ? Employee.find(params[:id]) : Employee.find(session[:user_id])

  #proceed to update
  if @employee.update_attributes(params[:employee])
    if params[:from_profile]=="true"
      flash[:notice] = "Success"
      redirect_to profile_path(@employee)
    else
      flash[:notice] = "Success"
      redirect_to edit_employee_path(@employee)
    end
  else
    # render "edit" and return
    if params[:from_profile]=="true"
      render "userviews/profile"
    else
      render "edit"
    end
    # render "userviews/profile" and return
  end

end

def destroy
  @employee = Employee.find(params[:id])
  @employee.status = 'D'
  @employee.save
  #@job_level.destroy
  redirect_to employees_path#, :notice => "Deleted"
end

private

  def admin_only
    unless current_user.is_admin == "Y"
      redirect_to userviews_path, :alert => "Access Denied"
    end
  end

  # def set_layout

  #   if current_user.is_admin == "Y"
  #     "admin"
  #   elsif current_user.role.downcase == "manager" || current_user.role.downcase == "employee"
  #     "user"
  #   end

  # end

end