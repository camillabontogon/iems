class UndertimesController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create, :update, :review_multiple]
  layout 'user'
  before_filter :check_session

  def index
    @employee = Employee.find(session[:user_id]);
    @undertimes = @employee.undertimes.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @undertime = Undertime.find(params[:id])
  end

  def new
    @undertime = Undertime.new
  end

  def create
    @employee = Employee.find(session[:user_id]);
    @undertime = Undertime.new(params[:undertime])
    @undertime.employee_id = @employee.id
    @undertime.status = "P"
    if @undertime.save
      @employee.superiors.each do |boss|
        UserNotifier.undertime_notification(@undertime,boss).deliver_now
      end
      redirect_to undertimes_path, :alert => "Successfully filed Undertime Request"
    else
      render "new"
    end
  end

  def destroy
    @undertime = Undertime.find(params[:id])
    @undertime.status = 'C'
    @undertime.save
    #@job_level.destroy
    redirect_to undertimes_path, :alert => "Undertime Request Cancelled"
  end

  def review_multiple
    if params[:undertime_ids]
      if params[:commit] == 'Deny'
        Undertime.where(:id => params[:undertime_ids]).update_all(status: 'D')
      else
        Undertime.where(:id => params[:undertime_ids]).update_all(status: 'A')
      end
      Undertime.where(:id => params[:undertime_ids]).update_all(reviewed_by: session[:user_id])

      if is_admin
        redirect_to admin_undertimes_path, :alert => "Successfully reviewed Undertime Requests"  
      else
        redirect_to review_undertimes_path, :alert => "Successfully reviewed Undertime Requests"
      end

    else

      if is_admin
        redirect_to admin_undertimes_path, :alert => "Did not select a request!"  
      else
        redirect_to review_undertimes_path, :alert => "Did not select a request!"
      end

    end
  end

  def edit
    @undertime = Undertime.find(params[:id])
  end

  def update
    @undertime = Undertime.find(params[:id])
    if @undertime.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @undertime.status= 'D'
      else
        @undertime.status= 'A'
      end
      @undertime.reviewed_by = session[:user_id]
      @undertime.save
      UserNotifier.undertime_approval_notification(@undertime).deliver_now
    else
      #optional message
    end
    redirect_to edit_undertime_path, :alert => "Successfully reviewed Undertime Request"
  end

  def email
    @undertime = Undertime.find(params[:id])
    if @undertime.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @undertime.status= 'D'
      else
        @undertime.status= 'A'
      end
      @undertime.reviewed_by = params[:reviewed_by]
      @undertime.save
      UserNotifier.undertime_approval_notification(@undertime).deliver_now
    else
      #optional message
    end

    if(session[:user_id] != nil)
      redirect_to review_undertimes_url
    else
      redirect_to login_url
    end
  end

end
