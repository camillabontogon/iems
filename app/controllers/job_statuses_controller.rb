class JobStatusesController < ApplicationController
  layout 'admin'
  before_filter :check_session
  before_filter :admin_only, :except => [:edit, :update, :show, :create, :destroy]

  def admin_only
    unless current_user.is_admin == "Y"
      redirect_to userviews_path, :alert => "Access Denied"
    end
  end

  def index
    @job_statuses = JobStatus.where(status: 'A')
  end

  def new
    @job_status = JobStatus.new
  end

  def create
    @job_status = JobStatus.new(params[:job_status])
    @job_status.status = 'A'
    if @job_status.save
      redirect_to job_statuses_path#, :notice => "job_level saved"
    else
      render "new"
    end
  end

  def edit
    @job_status = JobStatus.find(params[:id])
  end

  def update
    @job_status = JobStatus.find(params[:id])
    if @job_status.update_attributes(params[:job_status])
      redirect_to job_statuses_path#, :notice => "Updated"
    else
      render "edit"
    end
  end

  def destroy
    @job_status = JobStatus.find(params[:id])
    @job_status.status = 'D'
    @job_status.save
    redirect_to job_statuses_path#, :notice => "Deleted"
  end
end
