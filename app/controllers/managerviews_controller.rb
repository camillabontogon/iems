class ManagerviewsController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:index, :sort_pending]
  layout 'user'
  before_filter :manager_only, :check_session

  def manager_only
    if(session[:user_id] != nil)
      unless current_user.role == "Manager"
        redirect_to userviews_path, :alert => "Access Denied"
      end
    else
      redirect_to root_url
    end
  end

  def view_subordinates
    @employees = Employee.find(params[:id]).subordinates
  end

  def review_leaves
    @employee = Employee.find(session[:user_id])
    @leaves = Leave.joins("INNER JOIN approvers on leaves.employee_id = approvers.employee_id and approvers.approver_id = " + @employee.id.to_s).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_undertimes
    @employee = Employee.find(session[:user_id])
    @undertimes = Undertime.joins("INNER JOIN approvers on undertimes.employee_id = approvers.employee_id and approvers.approver_id = " + @employee.id.to_s).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_overtimes
    @employee = Employee.find(session[:user_id])
    @overtimes = Overtime.joins("INNER JOIN approvers on overtimes.employee_id = approvers.employee_id and approvers.approver_id = " + @employee.id.to_s).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_offsets
    @employee = Employee.find(session[:user_id])
    @offsets = Offset.joins("INNER JOIN approvers on offsets.employee_id = approvers.employee_id and approvers.approver_id = " + @employee.id.to_s).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_official_businesses
    @employee = Employee.find(session[:user_id])
    @obs = OfficialBusiness.joins("INNER JOIN approvers on official_businesses.employee_id = approvers.employee_id and approvers.approver_id = " + @employee.id.to_s).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_equipment_requests
    @employee = Employee.find(session[:user_id])
    @ers = EquipmentRequest.joins("INNER JOIN approvers on equipment_requests.employee_id = approvers.employee_id and approvers.approver_id = " + @employee.id.to_s).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def sort_pending
    if params["requests"]
      params["requests"].each do |det|
        request_type = det.split(":").first
        request_id = det.split(":").last.to_i
        puts request_type
        puts request_id.to_s
        case request_type
          when "Leave"
            @leave = Leave.find(request_id)
            if @leave.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @leave.status = 'D'
              else
                @leave.status = 'A'
                @leave.deduct_balance
              end
              @leave.reviewed_by = session[:user_id]
              @leave.save
              UserNotifier.leave_approval_notification(@leave).deliver_now
            else
              #optional message
            end
          when "Undertime"
            @undertime = Undertime.find(request_id)
            if @undertime.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @undertime.status= 'D'
              else
                @undertime.status= 'A'
              end
              @undertime.reviewed_by = session[:user_id]
              @undertime.save
              UserNotifier.undertime_approval_notification(@undertime).deliver_now
            else
              #optional message
            end
          when "Overtime"
            @overtime = Overtime.find(request_id)
            if @overtime.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @overtime.status= 'D'
              else
                @overtime.status= 'A'
              end
              @overtime.reviewed_by = session[:user_id]
              @overtime.save
              UserNotifier.overtime_approval_notification(@overtime).deliver_now
            else
              #optional message
            end
          when "Official Business"
            @ob = OfficialBusiness.find(request_id)
            if @ob.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @ob.status= 'D'
              else
                @ob.status= 'A'
              end
              @ob.reviewed_by = session[:user_id]
              @ob.save
              UserNotifier.ob_approval_notification(@ob).deliver_now
            else
              #optional message
            end
          when "Offset"
            @off = Offset.find(request_id)
            if @off.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @off.status= 'D'
                @off.offset_overtimes.each do |ot|
                  @overtime = Overtime.find(ot.overtime_id)
                  @overtime.status = 'P'
                  @overtime.save
                end
              else
                @off.status= 'A'
              end
              @off.reviewed_by = session[:user_id]
              @off.save
              UserNotifier.offset_approval_notification(@offset).deliver_now
            else
                #optional message
            end
          else
            @er = EquipmentRequest.find(request_id)
            if @er.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @er.status= 'D'
              else
                @er.status= 'A'
              end
              @er.reviewed_by = session[:user_id]
              @er.save
            else
              #optional message
            end
        end
      end
      redirect_to managerviews_path, :notice => "Successfully reviewed Requests"
    else
      redirect_to managerviews_path, :notice => "Did not select a request!"
    end

  end

  def index
    @connection = connect_to_db

    unionquery = File.read("#{Rails.root}/public/managerpending.txt") + current_user.id.to_s + "' order by unionquery.created_at desc";
    @results = @connection.connection.execute(unionquery)

    #data for csv
    @sample = CSV.generate do |csv|
      csv << ["from","something","to","something"]
      csv << ["EMPLOYEE NUMBER","LAST NAME","FIRST NAME","MANAGER","DEPARTMENT","BIOMETRICS ID","FALCO ID","DATE(s)","OT REGULAR DAY","OT REST DAY / HOLIDAY", "OT SPECIAL HOLIDAY on REST DAY","OT REGULAR HOLIDAY","OT REGULAR HOLIDAY on REST DAY","UNDERTIME","VACATION LEAVE","VL BALANCE","SICK LEAVE","SL BALANCE","OB DEPARTURE","OB TIME START","OB TIME END","OB ARRIVAL","OFFSET","HOLIDAY","TIME IN","TIME OUT","REMARK(s)"]
      @results.each(:as => :hash) do |row|
        csv << [row["request_type"],row["request_id"],row["created_at"],row["employee_id"],row["start_date"],row["end_date"],row["details"]]
      end
    end

    #generate csv
    respond_to do |format|
      format.html
      format.csv { send_data @sample }
      #, filename: "users-#{Date.today}.csv"
    end

  end
end