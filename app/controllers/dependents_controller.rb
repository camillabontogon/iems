class DependentsController < ApplicationController
  layout 'admin'
  before_filter :check_session

  def new
    @employee = Employee.find(params[:id])
    @dependent = @employee.dependents.build
    if current_user.id == params[:id].to_i
      render :layout => 'user'
    end
  end

  def create
    @employee = Employee.find(params[:id])
    @dependent = @employee.dependents.build(params[:dependent])
    @dependent.status = 'A'
    if @dependent.save
      if current_user.id != @employee.id
        redirect_to edit_employee_path(@employee)#, :notice => "Updated"
      else
        redirect_to profile_path(current_user)
      end
    else
      render "new"
    end
  end

  def edit
    @dependent = Dependent.find(params[:id])
    @employee = @dependent.employee
    if current_user.id == @employee.id
      render :layout => 'user'
    end
  end

  def update
    @dependent = Dependent.find(params[:id])
    @employee = @dependent.employee
    if @dependent.update_attributes(params[:dependent])
      if current_user.id != @employee.id
        redirect_to edit_employee_path(@employee)#, :notice => "Updated"
      else
        redirect_to profile_path(current_user)
      end
    else
      render "edit"
    end
  end

  def destroy
    @dependent = Dependent.find(params[:id])
    @dependent.status = 'D'
    @dependent.save
    @employee = @dependent.employee
    #@job_level.destroy
    redirect_to edit_employee_path(@employee)#, :notice => "Deleted"
  end
end
