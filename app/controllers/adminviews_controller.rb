class AdminviewsController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:index, :sort_pending, :post_reset_leaves]
  layout 'admin'
  before_filter :check_session
  before_filter :admin_only, :except => :generate_report

  def admin_only
    unless current_user.is_admin == "Y"
      redirect_to userviews_path, :alert => "Access Denied"
    end
  end

  def review_leaves
    @employee = Employee.find(session[:user_id])
    @leaves = Leave.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_undertimes
    @employee = Employee.find(session[:user_id])
    @undertimes = Undertime.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_overtimes
    @employee = Employee.find(session[:user_id])
    @overtimes = Overtime.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_offsets
    @employee = Employee.find(session[:user_id])
    @offsets = Offset.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_official_businesses
    @employee = Employee.find(session[:user_id])
    @obs = OfficialBusiness.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def review_equipment_requests
    @employee = Employee.find(session[:user_id])
    @ers = EquipmentRequest.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def sort_pending
    if params["requests"]
      params["requests"].each do |det|
        request_type = det.split(":").first
        request_id = det.split(":").last.to_i
        puts request_type
        puts request_id.to_s
        case request_type
          when "Leave"
            @leave = Leave.find(request_id)
            if @leave.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @leave.status = 'D'
              else
                @leave.status = 'A'
                @leave.deduct_balance
              end
              @leave.reviewed_by = session[:user_id]
              @leave.save
              UserNotifier.leave_approval_notification(@leave).deliver_now
            else
              #optional message
            end
          when "Undertime"
            @undertime = Undertime.find(request_id)
            if @undertime.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @undertime.status= 'D'
              else
                @undertime.status= 'A'
              end
              @undertime.reviewed_by = session[:user_id]
              @undertime.save
              UserNotifier.undertime_approval_notification(@undertime).deliver_now
            else
              #optional message
            end
          when "Overtime"
            @overtime = Overtime.find(request_id)
            if @overtime.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @overtime.status= 'D'
              else
                @overtime.status= 'A'
              end
              @overtime.reviewed_by = session[:user_id]
              @overtime.save
              UserNotifier.overtime_approval_notification(@overtime).deliver_now
            else
              #optional message
            end
          when "Official Business"
            @ob = OfficialBusiness.find(request_id)
            if @ob.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @ob.status= 'D'
              else
                @ob.status= 'A'
              end
              @ob.reviewed_by = session[:user_id]
              @ob.save
              UserNotifier.ob_approval_notification(@ob).deliver_now
            else
              #optional message
            end
          when "Offset"
            @off = Offset.find(request_id)
            if @off.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @off.status= 'D'
                @off.offset_overtimes.each do |ot|
                  @overtime = Overtime.find(ot.overtime_id)
                  @overtime.status = 'P'
                  @overtime.save
                end
              else
                @off.status= 'A'
              end
              @off.reviewed_by = session[:user_id]
              @off.save
              UserNotifier.offset_approval_notification(@offset).deliver_now
            else
              #optional message
            end
          else
            @er = EquipmentRequest.find(request_id)
            if @er.reviewed_by.nil?
              if params[:commit] == 'Deny'
                @er.status= 'D'
              else
                @er.status= 'A'
              end
              @er.reviewed_by = session[:user_id]
              @er.save
            else
              #optional message
            end
        end
      end
      redirect_to adminviews_path, :notice => "Successfully reviewed Requests"
    else
      redirect_to adminviews_path, :notice => "Did not select any request!"
    end
  end

  def index
    @connection = connect_to_db

    unionquery = File.read("#{Rails.root}/public/adminpending.txt") + " order by unionquery.created_at desc";
    @results = @connection.connection.execute(unionquery)

    @results.each(:as => :hash) do |row|
    end
  end

  def post_reset_leaves

    if current_user.email == params[:email]
      Employee.where(:job_status_id => 3).update_all(:sick_leave_balance => params[:sl].to_f)
      Employee.where(:job_status_id => 3).update_all(:vacation_leave_balance => params[:vl].to_f)

      respond_to do |format|
        format.json { render json: ["message"=>"ok"],status: :created }
      end
      
    else
      respond_to do |format|
        format.json { render json: ["message"=>"Invalid email!"], status: :unprocessable_entity}
      end
    end

  end

  def show_reset_leave_credits
    @user = Employee.find(session[:user_id])
  end

  

  def reports
  end

  def generate_report


    #filter by department
    if (params[:department]!="1")
      @employees = Employee.where(:business_unit_id => params[:department])
    else
      @employees = Employee.all
    end

    #filter by employee status
    @employees = @employees.where(:job_status => params[:emp_stat]).order(:last_name)

    start_date = params[:start_date]
    end_date = params[:end_date]

    @sample = CSV.generate do |csv|
      # csv << ["FROM",params[:start_date],"TO",params[:end_date]]
      csv << ["FROM",start_date,"TO",end_date]
      csv << ["EMPLOYEE NUMBER","LAST NAME","FIRST NAME","MANAGER","DEPARTMENT","BIOMETRICS ID","FALCO ID","DATE(s)","OT REGULAR DAY","OT REST DAY / HOLIDAY", "OT SPECIAL HOLIDAY on REST DAY","OT REGULAR HOLIDAY","OT REGULAR HOLIDAY on REST DAY","UNDERTIME","VACATION LEAVE","VL BALANCE","SICK LEAVE","SL BALANCE","OB DEPARTURE","OB TIME START","OB TIME END","OB ARRIVAL","OFFSET","HOLIDAY","TIME IN","TIME OUT","REMARK(s)"]

      @employees.each do |e|
        empdetails = [e.employee_no,e.last_name,e.first_name,(e.role=="Manager"? 1:0),BusinessUnit.find(e.business_unit_id).name,e.biometrics_id, (e.falco_id!="" ? e.falco_id : nil) ]
        #loop through date range
        vlbal = e.vacation_leave_balance
        slbal = e.sick_leave_balance

        (params[:start_date].to_date..params[:end_date].to_date).each do |date|
          #if date falls on a holiday
          holiday = 0
          special_days = SpecialDay.all.each do |sd|
            if sd.date == date
              holiday = 1
              break
            end
          end

          #go through each request type
          no_records = true
          remarks = ""
          one = 0
          two = 0
          three = 0
          four = 0
          five = 0
          sl = 0
          vl = 0
          undertime = 0

          if (params[:request_type].include? 'Overtime')
            ots = Overtime.ot_start(date.to_s).where(status: params[:request_status]).where(employee_id: e.id)
            if ots.size > 0
              day_type = ots.first.calendar_classification
              hours = ots.first.no_of_hours
              if day_type == 1
                one = hours
              elsif day_type ==2
                two = hours
              elsif day_type ==3
                three = hours
              elsif day_type == 4
                four = hours
              elsif day_type == 5
                five = hours
              end
              remarks+= " OT: " + ots.first.expected_output + "|"
              no_records = false
            end
          end
          if (params[:request_type].include? 'Undertime')
            undertimes = Undertime.undertime_date(date.to_s).where(status: params[:request_status]).where(employee_id: e.id)
            if undertimes.size > 0
              undertime = undertimes.first.date.strftime("%H:%M")
              remarks+= " UT: " + undertimes.first.reason + "|"
              no_records = false
            end
          end
          if (params[:request_type].include? 'Official Business')
            obs = OfficialBusiness.ob_date(date.to_s).where(status: params[:request_status]).where(employee_id: e.id)
            if obs.size > 0
              ob_start = obs.first.start_date.strftime("%H:%M")
              ob_end = obs.first.end_date.strftime("%H:%M")
              remarks+= " OB: " + obs.first.purpose + "|"
              no_records = false
            else
              ob_start = nil;
              ob_end = nil;
            end
          end
          if (params[:request_type].include? 'Offset')
            off = Offset.where(status: params[:request_status]).where(employee_id: e.id).where(date: date)
            if off.size > 0
              offset = off.first.offset_type
              remarks+= " Off: " + off.first.purpose + "|"
              no_records = false
            else
              offset = nil;
            end
          end
          if (params[:request_type].include? 'Leave')
            leaves = Leave.leave_date(date.to_s).where(status: params[:request_status]).where(employee_id: e.id)
            puts date.to_s

            if leaves.size > 0
              if (leaves.first.no_of_days%1 != 0)
                if (date==leaves.first.end_date) || (leaves.first.start_date==leaves.first.end_date)
                  if leaves.first.leave_type == "Sick Leave"
                    sl = 0.5
                    # slbal = -0.5
                  elsif leaves.first.leave_type == "Vacation Leave"
                    vl = 0.5
                    # vlbal = -0.5
                  end
                else
                  if leaves.first.leave_type == "Sick Leave"
                    sl = 1
                    # slbal = -1
                  elsif leaves.first.leave_type == "Vacation Leave"
                    vl = 1
                    # vlbal = -1
                  end
                end
              else
                if leaves.first.leave_type == "Sick Leave"
                  sl = 1
                  # slbal = -1
                elsif leaves.first.leave_type == "Vacation Leave"
                  vl = 1
                  # vlbal = -1
                end
              end
              remarks+= " Lv: " + leaves.first.reason + "|"
              no_records = false
            end
          end

          remarks = nil if remarks == ""
          if (no_records == true && params[:report_type] == "All Dates")
            csv << empdetails+[date.to_s]+[0,0,0,0,0]+[0]+[0]+[vlbal]+[0]+[slbal]+[nil,nil,nil,nil]+[nil]+[holiday]+[e.time_in.strftime("%H:%M")]+[e.time_out.strftime("%H:%M")]+[nil]
          elsif (no_records == true && params[:report_type] == "With Requests")
          else
            csv << empdetails+[date.to_s]+[one,two,three,four,five]+[undertime]+[vl]+[vlbal]+[sl]+[slbal]+[ob_start,ob_start,ob_end,ob_end]+[offset]+[holiday]+[e.time_in.strftime("%H:%M")]+[e.time_out.strftime("%H:%M")]+[remarks.squish]
          end

        end #end of date range
      end #end of employee loop
    end #end of csv

    respond_to do |format|
      fname = "IEMS_" + Time.now.strftime('%b-%d-%Y') + ".csv"

      format.html
      format.csv { send_data @sample, filename: fname }
    end


  end

end

