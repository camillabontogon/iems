class SessionsController < ApplicationController
  def create
    email = User.from_omniauth(env["omniauth.auth"])
    @employee = Employee.where(email: email).first
    if @employee.nil?
      flash[:error] = "It seems that you don't have an account with us yet. Contact HR for more information."
      redirect_to root_path
    else
      session[:user_id] = @employee.id
      role = Employee.find(session[:user_id]).getrole
      if role == "Manager"
        redirect_to managerviews_path
      else
        redirect_to userviews_path
      end
    end
  end

  def destroy
    session.destroy
  end
end