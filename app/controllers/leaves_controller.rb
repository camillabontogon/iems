class LeavesController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create, :update, :review_multiple]
  layout 'user'
  before_filter :check_session

  def index
    @leaves = Leave.where(employee_id: session[:user_id]).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @leave = Leave.find(params[:id])
    require 'date'
    @now = Date.today
  end

  def new
   @leave = Leave.new
   @employee = Employee.find(session[:user_id]);
  end

  def create
    @employee = Employee.find(session[:user_id]);
    @leave = Leave.new(params[:leave])
    @leave.employee_id = current_user.id
    @leave.status = "P"
    @leave.credits_used = 0
=begin
    #computation of no. of days
    if @leave.no_of_days!=0.5
      @leave.no_of_days = (@leave.end_date - @leave.start_date).to_i + 1
    elsif
      @leave.no_of_days = (@leave.end_date - @leave.start_date) + 0.5
    end
=end
    if @leave.save
      @leave.compute_days
      @leave.save
      @employee.superiors.each do |boss|
        UserNotifier.leave_notification(@leave,boss).deliver_now
      end
      redirect_to leaves_path, :alert => "Successfully filed Leave Request"
    else
      render "new"
    end
  end

  def destroy
    @leave = Leave.find(params[:id])
    #reverse leave credits if leave is already approved
    #cancel only if 2 days before
    @employee = Employee.find(@leave.employee_id)
    if @leave.status == 'A' && @leave.leave_type == "Sick Leave" && DateTime.now.to_date.strftime("%Y")==@leave.start_date.strftime("%Y")
      @employee.sick_leave_balance = @employee.sick_leave_balance + @leave.credits_used
      @employee.save
    elsif @leave.status == 'A' && @leave.leave_type == "Vacation Leave" && DateTime.now.to_date.strftime("%Y")==@leave.start_date.strftime("%Y")
      @employee.vacation_leave_balance = @employee.vacation_leave_balance + @leave.credits_used
      @employee.save
    end
    @leave.status = 'C'
    
    if @leave.save
      redirect_to leaves_path, :alert => "Leave Request Cancelled"
    else
      redirect_to leaves_path, :alert => @leave.errors
    end
  end

  def review_multiple
    if params[:leave_ids]
      if params[:commit] == 'Deny'
        Leave.where(:id => params[:leave_ids]).update_all(status: 'D')
      else
        Leave.where(:id => params[:leave_ids]).update_all(status: 'A')
        Leave.where(:id => params[:leave_ids]).each do |lv|
          lv.deduct_balance
          lv.save
        end
      end
      Leave.where(:id => params[:leave_ids]).update_all(reviewed_by: session[:user_id])
      if is_admin
        redirect_to admin_leaves_path, :alert => "Successfully reviewed Leave Requests"  
      else
        redirect_to review_leaves_path, :alert => "Successfully reviewed Leave Requests"
      end
    else
      if is_admin
        redirect_to admin_leaves_path, :alert => "Did not select a request!"
      else
        redirect_to review_leaves_path, :alert => "Did not select a request!"
      end

    end

  end

  def edit
    @leave = Leave.find(params[:id])
  end

  def update
    @leave = Leave.find(params[:id])
    if @leave.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @leave.status = 'D'
      else
        @leave.status = 'A'
        @leave.deduct_balance
      end

      if(params.has_key?(:review_by))#email approval
        @leave.reviewed_by = params[:reviewed_by]
      else
        @leave.reviewed_by = session[:user_id]
      end

      UserNotifier.leave_approval_notification(@leave).deliver_now
      @leave.save

    else
      #optional message
    end
      redirect_to leaves_path, :alert => "Successfully reviewed Leave Request"
  end

  def email
    @leave = Leave.find(params[:id])
    if @leave.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @leave.status = 'D'
      else
        @leave.status = 'A'
        @leave.deduct_balance
      end
      @leave.reviewed_by = params[:reviewed_by]
      @leave.save
      UserNotifier.leave_approval_notification(@leave).deliver_now
    else
      #optional message
    end

    if(session[:user_id] != nil)
      redirect_to review_leaves_url
    else
      redirect_to login_url
    end
  end
end
