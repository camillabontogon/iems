class UserviewsController < ApplicationController
  layout 'user'
  before_filter :check_session

  def profile
    # @employee = Employee.find(params[:id])
    if is_admin || current_user.role == "Manager"
      @employee = Employee.find(params[:id])
      @leaves = Leave.where(employee_id: params[:id]).where(status: 'A').all

      vl_count = 0
      sl_count = 0

      if @leaves.blank? == false
      @leaves.each do |l|
        if l.leave_type == 'Vacation Leave'
          vl_count = vl_count + l.no_of_days
        elsif l.leave_type == 'Sick Leave'
          sl_count = sl_count + l.no_of_days
        end
      end

      @vl = vl_count
      @sl = sl_count
      end

    else
      @employee = Employee.find(session[:user_id])
      @leaves = Leave.where(employee_id: session[:user_id]).where(status: 'A').all

      vl_count = 0
      sl_count = 0

      if @leaves.blank? == false
      @leaves.each do |l|
        if l.leave_type == 'Vacation Leave'
          vl_count = vl_count + l.no_of_days
        elsif l.leave_type == 'Sick Leave'
          sl_count = sl_count + l.no_of_days
        end
      end

      @vl = vl_count
      @sl = sl_count
      end 

    end
  end


  def index
    @connection = connect_to_db

    unionquery = File.read("#{Rails.root}/public/userpending.txt") + current_user.id.to_s + "' order by unionquery.created_at desc";
    @results = @connection.connection.execute(unionquery)

    @results.each(:as => :hash) do |row|
    end

  end
end
