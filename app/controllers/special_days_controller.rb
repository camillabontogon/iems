class SpecialDaysController < ApplicationController
  layout 'admin'
  before_filter :check_session
  before_filter :admin_only, :except => [:edit, :update, :show, :create, :destroy]

  def admin_only
    unless current_user.is_admin == "Y"
      redirect_to userviews_path, :alert => "Access Denied"
    end
  end

  def index
    @special_days = SpecialDay.where(status: 'A')
  end

  def new
    @special_day = SpecialDay.new
  end

  def create
    @special_day = SpecialDay.new(params[:special_day])
    @special_day.status = 'A'
    if @special_day.save
      redirect_to special_days_path
    else
      render "new"
    end
  end

  def edit
    @special_day = SpecialDay.find(params[:id])
  end

  def update
    @special_day = SpecialDay.find(params[:id])
    if @special_day.update_attributes(params[:special_day])
      redirect_to special_days_path#, :notice => "Updated"
    else
      render "edit"
    end
  end

  def destroy
    @special_day = SpecialDay.find(params[:id])
    @special_day.status = 'D'
    @special_day.save
    redirect_to special_days_path#, :notice => "Deleted"
  end
end
