class EquipmentRequestsController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create, :update, :review_multiple]
  layout 'user'

  before_filter :check_session

  def index
    @ers = EquipmentRequest.where(employee_id: session[:user_id]).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @er = EquipmentRequest.find(params[:id])
  end

  def new
  end

  def create
    @employee = Employee.find(session[:user_id]);
    @er = EquipmentRequest.new
    @er.purpose = params[:equipment_request][:purpose]
    @er.remark = params[:equipment_request][:remark]
    @er.target_delivery_date = params[:equipment_request][:target_delivery_date]
    @er.employee_id = @employee.id
    @er.status= "P"

    if @er.save
      params['equipment_request']['equipment_request_items_attributes'].each { |k,i|
        @eri = @er.equipment_request_items.build
        @eri.item = i['item']
        @eri.quantity = i['quantity']
        @eri.unit = i['unit']
        @eri.save
      }
      redirect_to equipment_requests_path, :alert => "Successfully filed Equipment Request"
    else
      # render "new"
      render json: ["status"=>"error", "error_list"=>@er.errors.full_messages]
    end
  end

  def destroy
    @er = EquipmentRequest.find(params[:id])
    @er.status = 'C'
    @er.save
    #@job_level.destroy
    redirect_to equipment_requests_path, :alert => "Equipment Request Cancelled"
  end


  def review_multiple
    if params[:er_ids]
      if params[:commit] == 'Deny'
        EquipmentRequest.where(:id => params[:er_ids]).update_all(status: 'D')
      else
        EquipmentRequest.where(:id => params[:er_ids]).update_all(status: 'A')
      end
      EquipmentRequest.where(:id => params[:er_ids]).update_all(reviewed_by: session[:user_id])

      if is_admin
        redirect_to admin_equipment_requests_path, :alert => "Successfully reviewed Leave Requests"  
      else
        redirect_to review_equipment_requests_path, :alert => "Successfully reviewed Leave Requests"
      end

    else

      if is_admin
        redirect_to admin_equipment_requests_path, :alert => "Did not select a request!"  
      else
        redirect_to review_equipment_requests_path, :alert => "Did not select a request!"
      end

    end
  end


  def edit
    @er = EquipmentRequest.find(params[:id])
  end

  def update
    @er = EquipmentRequest.find(params[:id])
    if @er.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @er.status= 'D'
      else
        @er.status= 'A'
      end
      @er.reviewed_by = session[:user_id]
      @er.save
    else
      #optional message
    end
    redirect_to edit_equipment_request_path, :alert => "Successfully reviewed Equipment Request"
  end
end
