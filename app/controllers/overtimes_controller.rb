class OvertimesController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create, :update, :review_multiple]
  layout 'user'
  before_filter :check_session

  def index
    @employee = Employee.find(session[:user_id]);
    @overtimes = @employee.overtimes.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @overtime = Overtime.find(params[:id])
  end

  def new
    @overtime = Overtime.new
  end

  def create
    @employee = Employee.find(session[:user_id]);
    @overtime = Overtime.new(params[:overtime])
    @overtime.employee_id = @employee.id
    @overtime.status = "P"

    ## WRONG IMPLEMENTATION
    ## THIS SHOULD BE IN THE MODEL
    @overtime.no_of_hours = ((@overtime.end_date - @overtime.start_date) / 1.hour)


    eleven_55pm = @overtime.end_date
    eleven_55pm = eleven_55pm.change(hour: 23, min: 55)

    #if until 1amS
    if params[:oneam] == "true" && @overtime.end_date == eleven_55pm
      @overtime.no_of_hours += 1
      @overtime.remark = @overtime.remark + " **Overtime til 1AM of the next day**"
    end

    require 'date'
    #loop through special days
    @special_days = SpecialDay.where(status:'A')
    day_type = nil
    @special_days.each do |sd|
      if @overtime.start_date.to_date == sd.date_before_type_cast
        day_type = sd.day_type
        puts "match" + day_type + sd.date_before_type_cast.to_s + @overtime.start_date.to_s
      end
    end

    #if saturday
    if @overtime.start_date.wday == 6
      @overtime.no_of_hours -= 5
    end

    if ((@overtime.start_date.wday == 6) || (@overtime.start_date.wday == 0))
      #if weekend
      puts day_type
      if day_type == "Regular Holiday"
        @overtime.calendar_classification = 5
      elsif day_type == "Special Holiday"
        @overtime.calendar_classification = 3
      else
        @overtime.calendar_classification = 2
      end
    else
      #if weekday
      if day_type == "Special Holiday"
        @overtime.calendar_classification = 2
      elsif day_type == "Regular Holiday"
        @overtime.calendar_classification = 4
      else
        @overtime.calendar_classification = 1
      end
    end
    puts "classification:" + @overtime.calendar_classification.to_s

    if @overtime.save
      @employee.superiors.each do |boss|
        UserNotifier.overtime_notification(@overtime,boss).deliver_now
      end
      redirect_to overtimes_path, :alert => "Successfully filed Overtime Request"
    else
      @overtime.start_date -= 8.hours #revert changes in chours
      @overtime.end_date -= 8.hours #revert changes in chours
      render "new"
    end
  end

  def destroy
    @overtime = Overtime.find(params[:id])
    @overtime.status = "C"
    @overtime.save
    redirect_to overtimes_path, :alert => "Overtime Request Cancelled"
  end


  def review_multiple
    if params[:overtime_ids]
      if params[:commit] == 'Deny'
        Overtime.where(:id => params[:overtime_ids]).update_all(status: 'D')
      else
        Overtime.where(:id => params[:overtime_ids]).update_all(status: 'A')
      end
      Overtime.where(:id => params[:overtime_ids]).update_all(reviewed_by: session[:user_id])

      if is_admin
        redirect_to admin_overtimes_path, :alert => "Successfully reviewed Overtime Requests"  
      else
        redirect_to review_overtimes_path, :alert => "Successfully reviewed Overtime Requests"
      end

    else

      if is_admin
        redirect_to admin_overtimes_path, :alert => "Did not select any request!"  
      else
        redirect_to review_overtimes_path, :alert => "Did not select any request!"
      end

    end
  end

  def edit
    @overtime = Overtime.find(params[:id])
  end

  def update
    @overtime = Overtime.find(params[:id])
    if @overtime.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @overtime.status= 'D'
      else
        @overtime.status= 'A'
      end
      @overtime.reviewed_by = session[:user_id]
      @overtime.save
      UserNotifier.overtime_approval_notification(@overtime).deliver_now
    else
      #optional message
    end
    redirect_to edit_overtime_path, :alert => "Successfully reviewed Overtime Request"
  end

  def email
    @overtime = Overtime.find(params[:id])
    if @overtime.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @overtime.status= 'D'
      else
        @overtime.status= 'A'
      end
      @overtime.reviewed_by = params[:reviewed_by]
      @overtime.save
      UserNotifier.overtime_approval_notification(@overtime).deliver_now
    else
      #optional message
    end

    if(session[:user_id] != nil)
      redirect_to review_overtimes_url
    else
      redirect_to login_url
    end
  end
end
