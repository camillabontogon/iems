class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # Form APIs, you may want to use :null_session instead.

  protect_from_forgery with: :exception, if: Proc.new { |c| c.request.format != 'application/json' }
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  before_filter :set_timezone
  helper_method :current_user

  def set_timezone
    #company = CompanyInfo.last
    #tz = company.time_zone
    #Time.zone = tz || ActiveSupport::TimeZone[tz]
  end

  def current_user
    @current_user ||= Employee.find(session[:user_id]) if session[:user_id]
  end

  def check_session
    if current_user == nil
      redirect_to root_url
    end
  end

  def is_admin
    if current_user.is_admin == "Y"
      return true
    else
      return false
    end
  end

  def set_layout

    if current_user.is_admin == "Y"
      return "admin"
    elsif current_user.role.downcase == "manager" || current_user.role.downcase == "employee"
      return "user"
    end

  end

  def connect_to_db

    connection = ActiveRecord::Base.establish_connection(
        adapter: "mysql2",
        encoding: "utf8",

        database: "iems_production",
        username: "root",
        password: "imaghine2016",
        #host: "iems-db.crux6xtz9pwg.us-west-2.rds.amazonaws.com"
    )
    return connection

  end


end
