class JobLevelsController < ApplicationController
  layout 'admin'
  before_filter :check_session
  before_filter :admin_only, :except => [:edit, :update, :show, :create, :destroy]

  def admin_only
    unless current_user.is_admin == "Y"
      redirect_to userviews_path, :alert => "Access Denied"
    end
  end
  def index
    @job_levels = JobLevel.where(status: 'A')
  end

  def show
    @job_level = JobLevel.find(params[:id])
  end

  def new
    @job_level = JobLevel.new
  end

  def create
    @job_level = JobLevel.new(params[:job_level])
    @job_level.status = 'A'
    if @job_level.save
      redirect_to job_levels_path#, :notice => "job_level saved"
    else
      render "new"
    end
  end

  def edit
    @job_level = JobLevel.find(params[:id])
  end

  def update
    @job_level = JobLevel.find(params[:id])
    if @job_level.update_attributes(params[:job_level])
      redirect_to job_levels_path#, :notice => "Updated"
    else
      render "edit"
    end
  end

  def destroy
    @job_level = JobLevel.find(params[:id])
    @job_level.status = 'D'
    @job_level.save
    #@job_level.destroy
    redirect_to job_levels_path#, :notice => "Deleted"
  end
end
