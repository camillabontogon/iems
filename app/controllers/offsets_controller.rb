class OffsetsController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create, :update, :review_multiple]
  layout 'user'
  before_filter :check_session

  def index
    @offsets = Offset.where(employee_id: session[:user_id]).order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @off = Offset.find(params[:id])
  end

  def new
  end

  def create
    @employee = Employee.find(session[:user_id]);
    @offset = Offset.new
    @offset.date = params[:offset][:date]
    @offset.purpose = params[:offset][:purpose]
    @offset.remark = params[:offset][:remark]
    @offset.offset_type = params[:offset][:type]
    @offset.employee_id = @employee.id
    @offset.status = 'P'

    offset_hours = 0
    if (@offset.offset_type == "Whole Day Mon-Thurs")
      offset_hours = 9
    elsif (@offset.offset_type == "Whole Day Friday")
      offset_hours = 8
    else
      offset_hours = 4
    end
    @offset.no_of_hours = offset_hours

    if @offset.save
      @employee.superiors.each do |boss|
        UserNotifier.offset_notification(@offset,boss).deliver_now
      end
      #use up Overtime
      params['offset']['ot_ids'].each { |k,i|
        @off_ot = @offset.offset_overtimes.build
        @off_ot.overtime_id = i.to_i;
        @ot = Overtime.find(i.to_i);
        if (offset_hours >= @ot.no_of_hours)
          @off_ot.hours_used = @ot.no_of_hours
          offset_hours = offset_hours - @ot.no_of_hours
        else
          @off_ot.hours_used = offset_hours
          offset_hours = 0
          puts offset_hours
        end
        @ot.status = 'U'
        @ot.save
        @off_ot.save
      }
      redirect_to offsets_path, :alert => "Successfully filed Offset Request"
    else
      # render "new"
       render json: ["status"=>"error", "error_list"=>@offset.errors.full_messages]
    end

  end

  def destroy
    @offset = Offset.find(params[:id])
    @offset.status = 'C'
    @offset.offset_overtimes.each do |off_ot|
      @ot = Overtime.find(off_ot.overtime_id)
      @ot.status = 'P'
      @ot.save
    end
    @offset.save
    redirect_to offsets_path, :alert => "Offset Request Cancelled"
  end

  def getot
    date = params[:start_date]
    @employee = Employee.find(session[:user_id])
    @overtimeresult = Overtime.find_by_sql ["SELECT overtimes.* FROM overtimes where employee_id = ? and status = 'P' and date(start_date) = ?", @employee.id, date.to_date.strftime("%F")]
    @overtime = @overtimeresult.first
    puts @overtime.id.to_s + " " + @overtime.start_date.to_s
    respond_to do |format|
      format.js { render :json => @overtime }
    end
  end


  def review_multiple
    if params[:offset_ids]
      if params[:commit] == 'Deny'
        Offset.where(:id => params[:offset_ids]).update_all(status: 'D')
        @offsets = Offset.where(:id => params[:offset_ids]).each do |off|
          off.offset_overtimes.each do |ot|
            @overtime = Overtime.find(ot.overtime_id)
            @overtime.status = 'P'
            @overtime.save
          end
        end
      else
        Offset.where(:id => params[:offset_ids]).update_all(status: 'A')
      end
      Offset.where(:id => params[:offset_ids]).update_all(reviewed_by: session[:user_id])

      if is_admin
        redirect_to admin_offsets_path, :alert => "Successfully reviewed Offset Requests"  
      else
        redirect_to review_offsets_path, :alert => "Successfully reviewed Offset Requests"
      end

    else

      if is_admin
        redirect_to admin_offsets_path, :alert => "Did not select a request!"  
      else
        redirect_to review_offsets_path, :alert => "Did not select a request!"
      end

    end
    
  end

  def edit
    @off = Offset.find(params[:id])
  end

  def update
    @off = Offset.find(params[:id])
    if @off.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @off.status= 'D'
        @off.offset_overtimes.each do |ot|
          @overtime = Overtime.find(ot.overtime_id)
          @overtime.status = 'P'
          @overtime.save
        end
      else
        @off.status= 'A'
      end
      @off.reviewed_by = session[:user_id]
      @off.save
      UserNotifier.offset_approval_notification(@off).deliver_now
    else
      #optional message
    end
    redirect_to edit_offset_path, :alert => "Successfully reviewed Offset Request"
  end

  def email
    @off = Offset.find(params[:id])
    if @off.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @off.status= 'D'
        @off.offset_overtimes.each do |ot|
          @overtime = Overtime.find(ot.overtime_id)
          @overtime.status = 'P'
          @overtime.save
        end
      else
        @off.status= 'A'
      end
      @off.reviewed_by = params[:reviewed_by]
      @off.save
      UserNotifier.offset_approval_notification(@off).deliver_now
    else
      #optional message
    end

    if(session[:user_id] != nil)
      redirect_to review_offsets_url
    else
      redirect_to login_url
    end
  end

end
