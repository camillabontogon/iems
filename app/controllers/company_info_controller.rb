class CompanyInfoController < ApplicationController
  layout 'admin'
  before_filter :check_session, :admin_only, :except => [:edit, :update]

  def admin_only
    unless current_user.is_admin == "Y"
      redirect_to userviews_path, :alert => "Access Denied"
    end
  end

  def index
    puts "Timenow " + Time.zone.now.to_s
    puts "Timezone " + Time.zone.name.to_s
    @company_info = CompanyInfo.find(1)
    @jobstatus = JobStatus.all

    @total_no = Employee.all.count
    @resigned = Employee.where(:job_status_id => 4).count
    @terminated = Employee.where(:job_status_id => 5).count

  end

  def edit
    @company_info = CompanyInfo.find(1)
  end

  def update
    @company_info = CompanyInfo.find(1)
    if @company_info.update_attributes(params[:company_info])
      redirect_to company_info_index_path
    else
      render "edit"
    end
  end

end
