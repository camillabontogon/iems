class EmergencyContactsController < ApplicationController
  layout 'admin'
  before_filter :check_session

  def new
    @employee = Employee.find(params[:id])
    @emergency_contact = @employee.emergency_contacts.build
    if current_user.id == params[:id].to_i
      render :layout => 'user'
    end
  end

  def create
    @employee = Employee.find(params[:id])
    @emergency_contact = @employee.emergency_contacts.build(params[:emergency_contact])
    @emergency_contact.status = 'A'
    if @emergency_contact.save
      if current_user.id != @employee.id
        redirect_to edit_employee_path(@employee)#, :notice => "Updated"
      else
        redirect_to profile_path(current_user)
      end
    else
      render "new"
    end
  end

  def edit
    @emergency_contact = EmergencyContact.find(params[:id])
    @employee = @emergency_contact.employee
    if current_user.id == @employee.id
      render :layout => 'user'
    end
  end

  def update
    @emergency_contact = EmergencyContact.find(params[:id])
    @employee = @emergency_contact.employee
    if @emergency_contact.update_attributes(params[:emergency_contact])
      if current_user.id != @employee.id
        redirect_to edit_employee_path(@employee)#, :notice => "Updated"
      else
        redirect_to profile_path(current_user)
      end
    else
      render "edit"
    end
  end

  def destroy
    @emergency_contact = EmergencyContact.find(params[:id])
    @emergency_contact.status = 'D'
    @emergency_contact.save
    @employee = @emergency_contact.employee
    #@job_level.destroy
    redirect_to edit_employee_path(@employee)#, :notice => "Deleted"
  end


end
