class OfficialBusinessesController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create, :update, :review_multiple]
  layout 'user'
  before_filter :check_session

  def index
    @employee = Employee.find(session[:user_id]);
    @obs = @employee.official_businesses.order("id DESC").all.paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @ob = OfficialBusiness.find(params[:id])
  end

  def new
    @ob = OfficialBusiness.new
  end

  def create
    @employee = Employee.find(session[:user_id]);

    if params.has_key?(:official_business) && !params[:official_business].empty?

      @ob = OfficialBusiness.new(params[:official_business])
      @ob.employee_id = @employee.id
      @ob.status= "P"

      respond_to do |format|
      
        if @ob.save

          @employee.superiors.each do |boss|
            UserNotifier.ob_notification(@ob,boss).deliver_now
          end

          format.html { redirect_to official_businesses_path, :alert => "Successfully filed Official Business Request" }
          format.json { render json: ["message"=>"ok"], status: :created }
        else
          format.html{ redirect_to new_official_business_path, :alert => @ob.errors.full_messages }
          format.json { render json: @ob.errors.full_messages, status: :unprocessable_entity }
        end

      end

    elsif params.has_key?(:official_businesses) && params[:official_businesses] != nil
      @ob_list = []
      params[:official_businesses].each do |i|
        @ob_list << OfficialBusiness.new(i)
      end

      begin 
        OfficialBusiness.transaction do
          @ob_list.each do |ob|
            ob.employee_id = @employee.id
            ob.status = "P"
            ob.save!
          end
        end

        #handle success
        respond_to do |format|
          format.json { render json: ["message"=>"ok"],status: :created }
        end

      #handle failure
      rescue ActiveRecord::RecordInvalid => exception
        # puts exception.message
        respond_to do |format|
          format.json { render json: ["message"=>exception.message] ,status: :unprocessable_entity }
        end

      end

    elsif params[:official_businesses] == nil
      respond_to do |format|
        format.json { render json: ["message"=>"Empty form!"], status: :unprocessable_entity}
      end
      
    end

  end

  def destroy
    @ob = OfficialBusiness.find(params[:id])
    @ob.status = 'C'
    @ob.save
    redirect_to official_businesses_path, :alert => "Official Business Request Cancelled"
  end


  def review_multiple
    if params[:ob_ids]
      if params[:commit] == 'Deny'
        OfficialBusiness.where(:id => params[:ob_ids]).update_all(status: 'D')
      else
        OfficialBusiness.where(:id => params[:ob_ids]).update_all(status: 'A')
      end
      OfficialBusiness.where(:id => params[:ob_ids]).update_all(reviewed_by: session[:user_id])

      if is_admin
        redirect_to admin_official_businesses_path, :alert => "Successfully reviewed Official Business Requests"  
      else
        redirect_to review_official_businesses_path, :alert => "Successfully reviewed Official Business Requests"
      end

    else

      if is_admin
        redirect_to admin_official_businesses_path, :alert => "Did not select a request!"  
      else
        redirect_to review_official_businesses_path, :alert => "Did not select a request!"
      end

    end
  end

  def edit
    @ob = OfficialBusiness.find(params[:id])
  end

  def update
    @ob = OfficialBusiness.find(params[:id])
    if @ob.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @ob.status= 'D'
      else
        @ob.status= 'A'
      end
      @ob.reviewed_by = session[:user_id]
      @ob.save
      UserNotifier.ob_approval_notification(@ob).deliver_now
    else
      #optional message
    end
    redirect_to edit_official_business_path, :alert => "Successfully reviewed Official Business Request"
  end


  def email
    @ob = OfficialBusiness.find(params[:id])
    if @ob.reviewed_by.nil?
      if params[:commit] == 'Deny'
        @ob.status= 'D'
      else
        @ob.status= 'A'
      end
      @ob.reviewed_by = params[:reviewed_by]
      @ob.save
      UserNotifier.ob_approval_notification(@ob).deliver_now
    else
      #optional message
    end

    if(session[:user_id] != nil)
      redirect_to review_official_businesses_url
    else
      redirect_to login_url
    end
  end
end
