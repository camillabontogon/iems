class BusinessUnitsController < ApplicationController
  layout 'admin'
  before_filter :check_session, :admin_only, :except => [:edit, :update, :show, :create, :destroy]

  def admin_only
    unless current_user.is_admin == "Y"
      redirect_to userviews_path, :alert => "Access Denied"
    end
  end

  def index
    @business_units = BusinessUnit.where(status: 'A')
  end

  def new
    @business_unit = BusinessUnit.new
  end

  def create
    @business_unit = BusinessUnit.new(params[:business_unit])
    @business_unit.status = 'A'
    if @business_unit.save
      redirect_to business_units_path#, :notice => "job_level saved"
    else
      render "new"
    end
  end

  def edit
    @business_unit = BusinessUnit.find(params[:id])
  end

  def update
    @business_unit = BusinessUnit.find(params[:id])
    if @business_unit.update_attributes(params[:business_unit])
      redirect_to business_units_path#, :notice => "Updated"
    else
      render "edit"
    end
  end

  def destroy
    @business_unit = BusinessUnit.find(params[:id])
    @business_unit.status = 'D'
    @business_unit.save
    #@job_level.destroy
    redirect_to business_units_path#, :notice => "Deleted"
  end
end
