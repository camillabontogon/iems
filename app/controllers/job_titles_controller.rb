class JobTitlesController < ApplicationController
layout 'admin'
before_filter :check_session
before_filter :admin_only, :except => [:edit, :update, :show, :create, :destroy]

def admin_only
  unless current_user.is_admin == "Y"
    redirect_to userviews_path, :alert => "Access Denied"
  end
end

def index
  @job_titles = JobTitle.where(status: 'A')
end

def new
  @job_title = JobTitle.new
end

def create
  @job_title = JobTitle.new(params[:job_title])
  @job_title.status = 'A'
  if @job_title.save
    redirect_to job_titles_path#, :notice => "job_level saved"
  else
    render "new"
  end
end

def edit
  @job_title = JobTitle.find(params[:id])
end

def update
  @job_title = JobTitle.find(params[:id])
  if @job_title.update_attributes(params[:job_title])
    redirect_to job_titles_path#, :notice => "Updated"
  else
    render "edit"
  end
end

def destroy
  @job_title = JobTitle.find(params[:id])
  @job_title.status = 'D'
  @job_title.save
  redirect_to job_titles_path#, :notice => "Deleted"

end
end