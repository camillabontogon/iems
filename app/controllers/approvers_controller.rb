class ApproversController < ApplicationController

  layout 'admin'
  before_filter :check_session

  def new
    @employee = Employee.find(params[:id])
    @approver = @employee.links_to_superior.build
  end

  def create
    @employee = Employee.find(params[:id])
    @approver = @employee.links_to_superior.build(params[:approver])
    if @approver.save
      redirect_to edit_employee_path(@employee)
    else
      render "new"
    end
  end

  def destroy
    @approver = Approver.find(params[:id])
    @employee = Employee.find(@approver.employee_id)
    @approver.destroy
    redirect_to edit_employee_path(@employee)#, :notice => "Deleted"
  end

end
