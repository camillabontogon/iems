class Undertime < ActiveRecord::Base
  attr_accessible :date, :nature, :reason, :remark
  validates :date, :nature, :reason, :presence => true
  validates_uniqueness_of :date, scope: :employee_id, conditions: -> {where.not(status: 'C')}
  belongs_to :employee
  scope :undertime_date, lambda {|d| where('date > ?', Date.parse(d).to_date).where('date < ?', (Date.parse(d) +1).to_date)}
end
