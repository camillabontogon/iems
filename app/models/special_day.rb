class SpecialDay < ActiveRecord::Base
  attr_accessible :date, :name, :day_type, :status
  validates :date, :name, :day_type, :presence => true
  validates_uniqueness_of :date, conditions: -> {where.not(status: 'C')}
end
