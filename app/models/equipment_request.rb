class EquipmentRequest < ActiveRecord::Base
  attr_accessible :purpose, :remark, :target_delivery_date, :equipment_request_items_attributes
  validates :target_delivery_date, :purpose, :presence => true

  has_many :equipment_request_items, :dependent => :destroy
  accepts_nested_attributes_for :equipment_request_items
  # validates :equipment_request_items, :presence => true

end
