class Leave < ActiveRecord::Base
  attr_accessible :leave_type, :reason, :remark, :no_of_days, :start_date, :end_date
  validates :leave_type, :reason, :start_date, :end_date, :presence =>  true
  validates_uniqueness_of :start_date, scope: :employee_id, conditions: -> {where.not(status: 'C')}
  belongs_to :employee
  validate :two_days_bef, :on => :create
  scope :leave_date, lambda {|d| where('(start_date = ? OR ? = end_date)OR(start_date < ? AND ? < end_date)', Date.parse(d).to_date, Date.parse(d).to_date,Date.parse(d).to_date, Date.parse(d).to_date)}
  scope :not_cancelled, -> {where.not(status: 'C')}
  validates "start_date", "end_date", :overlap => {
                :scope => "employee_id",
                :query_options => {:not_cancelled => nil},
                :message_content => "There is already a leave filed on this date"
            }
  def compute_days
  if self.no_of_days!=0.5
    self.no_of_days = (self.end_date - self.start_date).to_i + 1
  elsif
    self.no_of_days = (self.end_date - self.start_date) + 0.5
  end
end

def two_days_bef
  require 'date'
  if (self.leave_type == 'Vacation Leave' and Date.today > self.start_date-2)
    errors.add(:start_date, 'Vacation Leave must be filed 2 days before the start date')
  end
end

def deduct_balance
  @employee = Employee.find(self.employee_id)
  if self.leave_type == 'Sick Leave'
    if self.no_of_days <= @employee.sick_leave_balance
      @employee.sick_leave_balance = @employee.sick_leave_balance - self.no_of_days
      self.credits_used = self.no_of_days
    elsif self.no_of_days > @employee.sick_leave_balance
      self.credits_used = @employee.sick_leave_balance
      @employee.sick_leave_balance = 0
    end
  elsif self.leave_type == 'Vacation Leave'
    if self.no_of_days <= @employee.vacation_leave_balance
      @employee.vacation_leave_balance = @employee.vacation_leave_balance - self.no_of_days
      self.credits_used = self.no_of_days
    elsif self.no_of_days > @employee.vacation_leave_balance
      self.credits_used = @employee.vacation_leave_balance
      @employee.vacation_leave_balance = 0
    end
  end
  @employee.save
end

end
