class Overtime < ActiveRecord::Base
  attr_accessible :start_date, :end_date, :expected_output, :remark, :oneam
  validates :start_date, :end_date, :expected_output, :presence => true
  validates_uniqueness_of :start_date, scope: :employee_id, conditions: -> {where.not(status: 'C')}
  scope :not_cancelled, -> {where.not(status: 'C')}
  validates "start_date", "end_date", :overlap => {
      :scope => "employee_id",
      :query_options => {:not_cancelled => nil},
      :message_content => "There is already an overtime filed on this date"
  }
  after_initialize :set_default_value
  belongs_to :employee
  scope :ot_start, lambda {|d| where('start_date > ?', Date.parse(d).to_date).where('start_date < ?', (Date.parse(d) +1).to_date)}

  def set_default_value
    add_time = Time.now.friday? ? 1050.minutes : 1110.minutes
    self.start_date = Time.now.beginning_of_day + add_time if self.start_date.nil?
    self.end_date ||= self.start_date + 2.hours if self.end_date.nil?
  end

end
