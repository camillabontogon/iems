class Dependent < ActiveRecord::Base
  attr_accessible :employee_id, :name, :relationship, :birthday

  validates :name, :relationship, :presence => true
  validates :name, length:{maximum:100}
  validates :relationship, length:{maximum:50}

  belongs_to :employee

end
