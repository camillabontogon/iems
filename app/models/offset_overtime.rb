class OffsetOvertime < ActiveRecord::Base
  attr_accessible :overtime_id, :offset_id, :hours_used
  belongs_to :offset
end