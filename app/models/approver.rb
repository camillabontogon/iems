class Approver < ActiveRecord::Base
  attr_accessible :employee_id, :approver_id
  validates :approver_id, :uniqueness =>  {:scope => :employee_id}

  belongs_to :subordinate, :class_name => "Employee", :foreign_key => "employee_id"
  belongs_to :superior, :class_name => "Employee", :foreign_key => "approver_id"

end
