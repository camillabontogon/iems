class OfficialBusiness < ActiveRecord::Base
  attr_accessible :start_date, :end_date, :purpose, :client, :remark
  validates :start_date, :end_date, :purpose, :client, :presence =>  true
  validates :client, length:{maximum:50}
  validates :purpose, length:{maximum:250}


  validates_uniqueness_of :start_date, scope: :employee_id, conditions: -> {where("status != 'C' or status != 'D'")}
  belongs_to :employee
  scope :not_cancelled, -> {where("status != 'C' or status != 'D'")}
  validates "start_date", "end_date", :overlap => {
      :scope => "employee_id",
      :query_options => {:not_cancelled => nil},
      :message_content => "There is already an OB filed on this date"
  }
  scope :ob_date, lambda {|d| where('start_date > ?', Date.parse(d).to_date).where('start_date < ?', (Date.parse(d) +1).to_date)}
end
