class CompanyInfo < ActiveRecord::Base
  attr_accessible :name, :phone, :address, :zip, :tin, :pagibig, :philhealth, :sss, :time_zone, :status
  validates :name, :time_zone, :presence => true
end
