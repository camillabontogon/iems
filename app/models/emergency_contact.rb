class EmergencyContact < ActiveRecord::Base
  attr_accessible :employee_id, :name, :relationship, :home_no, :mobile_no, :office_no

  validates :name, :relationship, :mobile_no, :presence => true
  validates :name, length: {maximum:100}
  validates :relationship, length: {maximum:50}
  validates :mobile_no, length: {maximum:25}
  validates :home_no, length: {maximum:15}
  validates :office_no, length: {maximum:25}


  belongs_to :employee

end
