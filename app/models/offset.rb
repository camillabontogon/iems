class Offset < ActiveRecord::Base
  attr_accessible :date, :offset_type, :purpose, :remark
  validates :date, :offset_type, :purpose, :presence => true
	validates_uniqueness_of :date, scope: :employee_id, conditions: -> {where.not(status: 'C')}

	validates :offset_type, length:{maximum:30}
	validates :purpose, length:{maximum:250}
	validates :remark, length:{maximum:100}

  has_many :offset_overtimes, :dependent => :destroy
  accepts_nested_attributes_for :offset_overtimes
end