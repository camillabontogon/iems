class Employee < ActiveRecord::Base
  attr_accessible :employee_no, :date_employed, :date_regularized, :date_separated, :last_name, :first_name, :middle_name, :biometrics_id,
                  :falco_id, :gender, :street, :city, :province, :zip, :home_no, :mobile_no, :email, :email2, :birthday,
                  :time_in, :time_out, :tin, :pagibig, :philhealth, :sss, :vacation_leave_balance, :sick_leave_balance, :role, :is_admin,
                  :job_status_id, :job_title_id, :job_level_id, :business_unit_id, :from_profile, :status, :vacation_leave_credit, :sick_leave_credit

  validates :employee_no, :first_name, :last_name, :middle_name, :role, :email, :presence => true
  validates :employee_no, :uniqueness =>  true
  validates :first_name, :last_name, :middle_name, length: {maximum:100}
  validates :pagibig, length: {maximum:20}
  validates :philhealth, length: {maximum:20}
  validates :sss, length: {maximum:20}
  validates :tin, length: {maximum:20}
  validates :street, length: {maximum:100}
  validates :city, length: {maximum:50}
  validates :province, length: {maximum:50}
  validates :zip, length:{maximum:20}
  validates :home_no, length:{maximum:15}
  validates :mobile_no, length:{maximum:25}
  validates :email, :email2, length:{maximum:50}

  belongs_to :business_unit
  belongs_to :job_level
  belongs_to :job_title
  belongs_to :job_status

  has_many :emergency_contacts
  has_many :dependents
  has_many :undertimes
  has_many :leaves
  has_many :overtimes
  has_many :official_businesses

  has_many :links_to_superior, :class_name => "Approver", :foreign_key => "employee_id"
  has_many :superiors, :class_name => "Employee", :through => :links_to_superior

  has_many :links_to_subordinates, :class_name => "Approver", :foreign_key => "approver_id"
  has_many :subordinates, :class_name => "Employee", :through => :links_to_subordinates

  def firstlastname
    "#{first_name} #{last_name}"
  end

  def getrole
    "#{role}"
  end

  def getis_admin
    "#{is_admin}"
  end

  def self.to_csv
    attributes = %w{id first_name last_name}
    CSV.generate(headers: true) do |csv|
      csv << attributes
      all.each do |user|
        csv << user.attributes.values_at(*attributes)
      end
    end
  end

end
