class JobStatus < ActiveRecord::Base
  attr_accessible :name, :status
  validates :name, :presence => true, :uniqueness => true

  has_many :employees
end
