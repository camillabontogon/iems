class JobLevel < ActiveRecord::Base
  attr_accessible :level, :rank, :grade, :status
  validates :level, :rank, :grade, :presence => true

  has_many :employees

  def leveltos
    "#{rank} - #{level} - #{grade}"
  end
end
