class EquipmentRequestItem < ActiveRecord::Base
  attr_accessible :item, :quantity, :unit
  validates :item, :quantity, :unit, :presence => true
  belongs_to :equipment_request
end
