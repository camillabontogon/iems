class JobTitle < ActiveRecord::Base
  attr_accessible :name, :description, :status
  validates :name, :presence => true, :uniqueness => true
  has_many :employees

end
