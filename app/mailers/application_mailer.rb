class ApplicationMailer < ActionMailer::Base
  default from: "iripple.iems@gmail.com"
  layout 'mailer'
end
