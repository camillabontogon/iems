class UserNotifier < ApplicationMailer
  helper MailerHelper
  #default :from => 'camillabontogon@gmail.com'
  default :from => 'iripple.iems@gmail.com'

  def leave_notification(leave,boss)
    @leave = leave
      puts boss.firstlastname
      puts boss.id
      @approver_id = boss.id
      mail( :to => boss.email,
            :subject => 'iEMS Notification: Leave Request from ' + Employee.find(@leave.employee_id).firstlastname  )
  end

  def leave_approval_notification(leave)
    @leave = leave
      @employee = Employee.find(@leave.employee_id)
      @reviewed_by = Employee.find(@leave.reviewed_by)

      if @leave.status == 'D'
        @status = 'Denied'
      elsif @leave.status == 'A'
        @status = 'Approved'
      end

      mail( :to => @employee.email,
            :subject => 'iEMS Notification: Leave Request ' + @status)
  end

  def undertime_notification(undertime,boss)
    @undertime = undertime
    puts boss.firstlastname
    puts boss.id
    @approver_id = boss.id
    mail( :to => boss.email,
          :subject => 'iEMS Notification: Undertime Request from ' + Employee.find(@undertime.employee_id).firstlastname  )
  end

  def undertime_approval_notification(undertime)
    @undertime = undertime
      @employee = Employee.find(@undertime.employee_id)
      @reviewed_by = Employee.find(@undertime.reviewed_by)

      if @undertime.status == 'D'
        @status = 'Denied'
      elsif @undertime.status == 'A'
        @status = 'Approved'
      end

      mail( :to => @employee.email,
            :subject => 'iEMS Notification: Undertime Request ' + @status)
  end

  def ob_notification(ob,boss)
    @ob = ob
    puts boss.firstlastname
    puts boss.id
    @approver_id = boss.id
    mail( :to => boss.email,
          :subject => 'iEMS Notification: Official Business Request from ' + Employee.find(@ob.employee_id).firstlastname  )
  end

  def ob_approval_notification(ob)
    @ob = ob
      @employee = Employee.find(@ob.employee_id)
      @reviewed_by = Employee.find(@ob.reviewed_by)

      if @ob.status == 'D'
        @status = 'Denied'
      elsif @ob.status == 'A'
        @status = 'Approved'
      end

      mail( :to => @employee.email,
            :subject => 'iEMS Notification: Official Business Request ' + @status)
  end


  def offset_notification(off,boss)
    @off = off
    puts boss.firstlastname
    puts boss.id
    @approver_id = boss.id
    mail( :to => boss.email,
          :subject => 'iEMS Notification: Offset Request from ' + Employee.find(@off.employee_id).firstlastname  )
  end

  def offset_approval_notification(off)
    @off = off
      @employee = Employee.find(@off.employee_id)
      @reviewed_by = Employee.find(@off.reviewed_by)

      if @off.status == 'D'
        @status = 'Denied'
      elsif @off.status == 'A'
        @status = 'Approved'
      end

      mail( :to => @employee.email,
            :subject => 'iEMS Notification: Offset Request ' + @status)
  end

  def overtime_notification(ot,boss)
    @ot = ot
    puts boss.firstlastname
    puts boss.id
    @approver_id = boss.id
    mail( :to => boss.email,
          :subject => 'iEMS Notification: Overtime Request from ' + Employee.find(@ot.employee_id).firstlastname  )
  end

  def overtime_approval_notification(ot)
    @ot = ot
      @employee = Employee.find(@ot.employee_id)
      @reviewed_by = Employee.find(@ot.reviewed_by)

      if @ot.status == 'D'
        @status = 'Denied'
      elsif @ot.status == 'A'
        @status = 'Approved'
      end

      mail( :to => @employee.email,
            :subject => 'iEMS Notification: Overtime Request ' + @status)
  end

end
