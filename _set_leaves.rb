file = File.open("_leave_credit.csv", "r").readlines.each do |line|
	# emp_num,vl_balance,sl_balance
	emp_array = line.split(",")
	
	Employee.find_by(employee_no: emp_array[0]).update(vacation_leave_credit: emp_array[1], sick_leave_credit: emp_array[2], vacation_leave_balance: 0, sick_leave_balance: 0)

end