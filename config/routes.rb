Rails.application.routes.draw do
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
  get 'login', to: 'home#show', as: 'login'
  get '/', to: 'home#show', as: 'home'


  resources :sessions, only: [:create, :destroy]
  resource :home, only: [:show]

  root to: "home#show"
  get 'sessions/create'
  get 'sessions/destroy'
  get 'home/show'

  resources :job_levels
  resources :job_statuses
  resources :job_titles
  resources :company_info
  resources :business_units
  resources :employees
  resources :login
  resources :emergency_contacts
  resources :dependents
  resources :approvers
  resources :undertimes
  resources :leaves
  resources :overtimes
  resources :official_businesses
  resources :offsets
  resources :equipment_requests
  resources :managerviews
  resources :adminviews
  resources :userviews
  resources :special_days
  get "get_ot", to: "offsets#getot"

  post "employee_search", to: "employees#search"
  get "employee_search", to: "employees#search"

  post "add_sl", to: "employees#add_sick_leave_credit"
  post "add_vl", to: "employees#add_vacation_leave_credit"

  get "review_leaves", to: "managerviews#review_leaves", as: 'review_leaves'
  get "review_undertimes", to: "managerviews#review_undertimes", as: 'review_undertimes'
  get "review_overtimes", to: "managerviews#review_overtimes", as: 'review_overtimes'
  get "review_offsets", to: "managerviews#review_offsets", as: 'review_offsets'
  get "review_official_businesses", to: "managerviews#review_official_businesses", as: 'review_official_businesses'
  get "review_equipment_requests", to: "managerviews#review_equipment_requests", as: 'review_equipment_requests'

  get "admin_leaves", to: "adminviews#review_leaves", as: 'admin_leaves'
  get "admin_undertimes", to: "adminviews#review_undertimes", as: 'admin_undertimes'
  get "admin_overtimes", to: "adminviews#review_overtimes", as: 'admin_overtimes'
  get "admin_offsets", to: "adminviews#review_offsets", as: 'admin_offsets'
  get "admin_official_businesses", to: "adminviews#review_official_businesses", as: 'admin_official_businesses'
  get "admin_equipment_requests", to: "adminviews#review_equipment_requests", as: 'admin_equipment_requests'

  put "review_multiple_leaves", to: "leaves#review_multiple", as: 'review_multiple_leaves'
  put "review_multiple_undertimes", to: "undertimes#review_multiple", as: 'review_multiple_undertimes'
  put "review_multiple_overtimes", to: "overtimes#review_multiple", as: 'review_multiple_overtimes'
  put "review_multiple_offsets", to: "offsets#review_multiple", as: 'review_multiple_offsets'
  put "review_multiple_official_businesses", to: "official_businesses#review_multiple", as: 'review_multiple_official_businesses'
  put "review_multiple_equipment_requests", to: "equipment_requests#review_multiple", as: 'review_multiple_equipment_requests'

  get "profile/:id", to: "userviews#profile", as: 'profile'
  get "/view_subordinates/:id", to: "managerviews#view_subordinates", as: 'view_subordinates'

  put "email_leave/:id", to: "leaves#email", as: 'email_leave'
  put "email_undertime/:id", to: "undertimes#email", as: 'email_undertime'
  put "email_overtime/:id", to: "overtimes#email", as: 'email_overtime'
  put "email_ob/:id", to: "official_businesses#email", as: 'email_ob'
  put "email_offset/:id", to: "offsets#email", as: 'email_offset'

  post "manager/review/pending", to:"managerviews#sort_pending", as: 'manager_sort_pending'
  post "admin/review/pending", to:"adminviews#sort_pending", as: 'admin_sort_pending'

  get "/reports", to:"adminviews#reports", as: 'reports'
  get "/generate_report.csv", to: "adminviews#generate_report", as: 'download_report'
  post "/generate_report", to: "adminviews#generate_report", as: 'generate_report'

  get "reset_leaves", to: "adminviews#show_reset_leave_credits"
  post "reset_leave_credits", to: "adminviews#post_reset_leaves"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
