-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: iems-db.crux6xtz9pwg.us-west-2.rds.amazonaws.com    Database: iems_production
-- ------------------------------------------------------
-- Server version	5.5.46-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `approvers`
--

DROP TABLE IF EXISTS `approvers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `approvers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `approver_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_approvers_on_employees_id` (`employee_id`),
  KEY `fk_approver_id` (`approver_id`),
  CONSTRAINT `fk_approver_id` FOREIGN KEY (`approver_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `approvers`
--

LOCK TABLES `approvers` WRITE;
/*!40000 ALTER TABLE `approvers` DISABLE KEYS */;
INSERT INTO `approvers` VALUES (15,17,'2016-08-09 09:38:33','2016-08-09 09:38:33',18),(16,19,'2016-08-09 09:38:47','2016-08-09 09:38:47',18),(17,20,'2016-08-09 09:38:58','2016-08-09 09:38:58',18),(19,24,'2016-08-15 10:57:41','2016-08-15 10:57:41',23),(20,25,'2016-08-15 11:00:06','2016-08-15 11:00:06',23),(21,14,'2016-08-24 05:10:14','2016-08-24 05:10:14',18),(22,27,'2016-12-12 02:33:53','2016-12-12 02:33:53',28),(23,28,'2016-12-12 02:42:55','2016-12-12 02:42:55',28);
/*!40000 ALTER TABLE `approvers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_units`
--

DROP TABLE IF EXISTS `business_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_units`
--

LOCK TABLES `business_units` WRITE;
/*!40000 ALTER TABLE `business_units` DISABLE KEYS */;
INSERT INTO `business_units` VALUES (0,'All','H','2016-08-08 14:16:05','2016-08-08 14:16:05'),(9,'Barter Local - Support','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(10,'Barter Local - Project','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(11,'Executive','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(12,'HR & Admin','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(13,'Imonggo','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(14,'Project - Ayagold','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(15,'Project - Prince','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(16,'Project - Barter CX','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(17,'Project - Petron','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(18,'International Business - Thailand','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(19,'Marketing & Hardware','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(20,'Finance','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(21,'Product','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(22,'ATVI','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(23,'Synext (Nexus)','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(24,'Imaghine','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(25,'Sales','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(26,'Enterprise - Primer','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(27,'Project','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(28,'Enterprise - Prince','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(29,'Business Operations','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(30,'QA and Training','A','2016-08-08 14:16:05','2016-08-08 14:16:05'),(31,'Imonggo International','A','2016-08-08 14:16:05','2016-08-08 14:16:05');
/*!40000 ALTER TABLE `business_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_infos`
--

DROP TABLE IF EXISTS `company_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `tin` varchar(20) DEFAULT NULL,
  `pagibig` varchar(20) DEFAULT NULL,
  `philhealth` varchar(20) DEFAULT NULL,
  `sss` varchar(20) DEFAULT NULL,
  `time_zone` varchar(50) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_infos`
--

LOCK TABLES `company_infos` WRITE;
/*!40000 ALTER TABLE `company_infos` DISABLE KEYS */;
INSERT INTO `company_infos` VALUES (1,'iRipple','','','','','','','','UTC',NULL,'2016-08-08 14:16:05','2016-08-09 10:10:03');
/*!40000 ALTER TABLE `company_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependents`
--

DROP TABLE IF EXISTS `dependents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_dependents_on_employee_id` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependents`
--

LOCK TABLES `dependents` WRITE;
/*!40000 ALTER TABLE `dependents` DISABLE KEYS */;
INSERT INTO `dependents` VALUES (5,26,'RAMON A SAHAGUN','HUSBAND','1959-08-30','A','2016-08-17 07:10:41','2016-08-17 07:10:41'),(6,30,'Tester tester','Moma','2016-11-28','D','2016-12-21 05:55:49','2016-12-21 05:56:23'),(7,28,'Mama ','Mother','2016-12-22','A','2016-12-21 09:10:26','2016-12-21 09:10:26');
/*!40000 ALTER TABLE `dependents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emergency_contacts`
--

DROP TABLE IF EXISTS `emergency_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emergency_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `home_no` varchar(15) DEFAULT NULL,
  `mobile_no` varchar(25) DEFAULT NULL,
  `office_no` varchar(25) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_emergency_contacts_on_employee_id` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emergency_contacts`
--

LOCK TABLES `emergency_contacts` WRITE;
/*!40000 ALTER TABLE `emergency_contacts` DISABLE KEYS */;
INSERT INTO `emergency_contacts` VALUES (9,26,'RAMON A SAHAGUN','HUSBAND','921-1350','+639178943008','N/A','A','2016-08-17 07:10:14','2016-08-17 07:10:14'),(10,26,'JONATHAN AFRICA','BROTHER','3721797','+639178460910','','A','2016-08-17 07:17:22','2016-08-17 07:17:22'),(11,28,'jkhkjhkjhjkhkjhjkhkjhkjhhjkhjkhkjhkjh','asda','hjkhkj','asda','','D','2016-11-18 06:13:08','2016-11-18 06:14:20'),(12,28,'Elle Arroyo','In-law','ad','ad','ad','A','2016-11-18 08:26:02','2016-11-18 08:26:02'),(13,30,'Tester mom','mother','ad','aw','aw','D','2016-12-21 05:53:53','2016-12-21 05:54:14');
/*!40000 ALTER TABLE `emergency_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_employed` date DEFAULT NULL,
  `date_regularized` date DEFAULT NULL,
  `date_separated` date DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `business_unit_id` int(11) DEFAULT NULL,
  `job_title_id` int(11) DEFAULT NULL,
  `job_level_id` int(11) DEFAULT NULL,
  `job_status_id` int(11) DEFAULT NULL,
  `biometrics_id` varchar(10) DEFAULT NULL,
  `falco_id` varchar(10) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `home_no` varchar(15) DEFAULT NULL,
  `mobile_no` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `time_in` time DEFAULT NULL,
  `time_out` time DEFAULT NULL,
  `role` varchar(15) DEFAULT NULL,
  `is_admin` varchar(1) DEFAULT NULL,
  `tin` varchar(20) DEFAULT NULL,
  `pagibig` varchar(20) DEFAULT NULL,
  `philhealth` varchar(20) DEFAULT NULL,
  `sss` varchar(20) DEFAULT NULL,
  `vacation_leave_balance` float DEFAULT NULL,
  `sick_leave_balance` float DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `employee_no` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_employees_on_business_unit_id` (`business_unit_id`),
  KEY `index_employees_on_job_title_id` (`job_title_id`),
  KEY `index_employees_on_job_level_id` (`job_level_id`),
  KEY `index_employees_on_job_status_id` (`job_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (14,NULL,NULL,NULL,'Bontogon','Camilla Denise','Fonollera',24,6,12,10,'','','M','','','','','','','cbontogon@iripple.net','',NULL,'08:30:00','18:30:00','Employee','Y','','','','',0,0,'A','2016-08-08 14:16:05','2016-08-12 02:46:03','20160601'),(15,NULL,NULL,NULL,'Mendoza','Diana','Codilan',12,36,12,9,'','','M','','','','','','','dmendoza@iripple.com','',NULL,'08:30:00','18:30:00','Manager','Y','','','','',NULL,NULL,'A','2016-08-08 14:16:05','2016-08-24 05:10:31','20131001'),(16,NULL,NULL,NULL,'Cañete','Carol Ann','Rodriguez',12,49,12,9,'','','M','','','','','','','ccanete@iripple.com','',NULL,'08:30:00','18:30:00','Employee','Y','','','','',7,7,'A','2016-08-08 14:16:05','2016-08-12 03:08:02','20140616'),(17,NULL,NULL,NULL,'Santos','Edmundo III','Merlan',24,6,12,9,'','','M','','','','','','','esantos@iripple.net','',NULL,'08:30:00','18:30:00','Employee','Y','','','','',3,6,'A','2016-08-08 16:10:45','2016-10-10 06:42:50','20150629'),(18,NULL,NULL,NULL,'Bonita','Reynaldo Jr.','Catuira',24,50,12,9,'','','M','','','','','','','abonita@iripple.com','',NULL,'08:30:00','18:30:00','Manager','N','','','','',7,7,'A','2016-08-09 09:34:25','2016-08-12 03:07:35','20111201'),(19,NULL,NULL,NULL,'Mendoza','Cris Paula','Ignacio',24,39,12,10,'','','M','','','','','','','cmendoza@iripple.net','',NULL,'08:30:00','18:30:00','Employee','N','','','','',0,0,'A','2016-08-09 09:35:39','2016-08-12 03:07:19','20160705'),(20,NULL,NULL,NULL,'Oliveros','Lanz Andre','Albaran',24,66,12,9,'','','M','','','','','','','loliveros@iripple.net','',NULL,'08:30:00','18:30:00','Employee','N','','','','',0,0,'A','2016-08-09 09:36:52','2016-12-08 03:42:37','20150630'),(21,NULL,NULL,NULL,'Balingit','Barbara','Sanchez',12,32,12,9,'','','M','','','','','','','bbalingit@iripple.com','',NULL,'08:30:00','18:30:00','Employee','Y','','','','',7,7,'A','2016-08-12 02:03:24','2016-08-12 03:06:33','20110201'),(22,NULL,NULL,NULL,'Tablizo','Karlo','Aldave',11,74,12,9,'','','M','','','','','','','karlo.a.tablizo@gmail.com','',NULL,'08:30:00','18:30:00','Manager','N','','','','',7,7,'A','2016-08-12 05:15:56','2016-08-16 05:59:28','20100103'),(23,NULL,NULL,NULL,'Soriano','Josif Hans','Salazar',27,23,12,9,'','','M','','','','','','','hsoriano@iripple.com','',NULL,'08:30:00','18:30:00','Manager','N','','','','',7,7,'A','2016-08-15 10:53:59','2016-08-15 10:54:54','20120603'),(24,NULL,NULL,NULL,'Muncal','Ardo','Reambillo',16,22,12,9,'','','M','','','','','','','amuncal@iripple.net','',NULL,'08:30:00','18:30:00','Employee','N','','','','',7,7,'A','2016-08-15 10:56:30','2016-08-15 10:57:31','20150504'),(25,NULL,NULL,NULL,'Golo','Rianna Rae','Makapugay',16,22,12,9,'','','M','','','','','','','rgolo@iripple.net','',NULL,'08:30:00','18:30:00','Employee','N','','','','',7,7,'A','2016-08-15 10:59:07','2016-08-15 10:59:57','20150511'),(26,'2016-06-01',NULL,NULL,'Sahagun','Caroline','Africa',11,79,12,9,'','','F','55 Benito Soliven III Ext. , Loyola Grand Villas','Quezon CIty','NCR','1008','921-1350','+639178801701','csahagun@iripple.com','casahagun@gmail.com','1961-08-01','08:30:00','18:30:00','Manager','N','101-607-391','0002237402 01','19-079198021-0','03-7050943-5',7,7,'A','2016-08-16 05:16:14','2016-08-17 07:14:52','20160603'),(27,NULL,NULL,NULL,'Chua','Patricia Kathryn','Ong',30,75,12,10,'','','F','','','','','','','pchua@iripple.net','',NULL,'08:30:00','18:30:00','Employee','N','','','','',0,0,'A','2016-08-16 05:18:31','2016-08-16 05:27:21','20160222'),(28,NULL,NULL,NULL,'Tongco','Camille','De Guzman',13,8,12,10,'','','M','','','','','','','choie@imonggo.com','','2017-01-19','08:30:00','18:30:00','Manager','Y','','kd;a','','',0,0,'A','2016-11-17 01:30:47','2016-12-21 08:59:10','187'),(29,NULL,NULL,NULL,'Tanqueco','Mikee Dorina','Tanqueco',13,6,12,9,'','','F','','','','','','','mtanqueco@iripple.net','mikee.tanqueco@uap.asia',NULL,'08:30:00','18:30:00','Manager','Y','','','','',7,7,'A','2016-11-17 01:36:10','2016-11-18 07:37:25','30'),(30,NULL,NULL,NULL,'Test','Tester','Test',9,6,12,9,'','','M','','','','','','','','',NULL,'08:30:00','18:30:00','Employee','N','','ASasASaAasASasA','','',0,0,'A','2016-12-12 02:36:42','2016-12-21 05:52:21','090909');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment_request_items`
--

DROP TABLE IF EXISTS `equipment_request_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment_request_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_request_id` int(11) DEFAULT NULL,
  `item` varchar(100) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `unit` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_equipment_request_items_on_equipment_request_id` (`equipment_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment_request_items`
--

LOCK TABLES `equipment_request_items` WRITE;
/*!40000 ALTER TABLE `equipment_request_items` DISABLE KEYS */;
INSERT INTO `equipment_request_items` VALUES (9,8,'Laptop',3,'2016-12-12 08:29:11','2016-12-12 08:29:11','pc'),(10,8,'Laptop',3,'2016-12-12 08:29:11','2016-12-12 08:29:11','pc');
/*!40000 ALTER TABLE `equipment_request_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment_requests`
--

DROP TABLE IF EXISTS `equipment_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `target_delivery_date` date DEFAULT NULL,
  `purpose` varchar(250) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `confirmation_status` varchar(1) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `reviewed_by` int(11) DEFAULT NULL,
  `confirmed_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_equipment_requests_on_employee_id` (`employee_id`),
  KEY `confirmed_by` (`confirmed_by`),
  KEY `reviewed_by` (`reviewed_by`),
  CONSTRAINT `equipment_requests_ibfk_1` FOREIGN KEY (`confirmed_by`) REFERENCES `employees` (`id`),
  CONSTRAINT `equipment_requests_ibfk_2` FOREIGN KEY (`reviewed_by`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment_requests`
--

LOCK TABLES `equipment_requests` WRITE;
/*!40000 ALTER TABLE `equipment_requests` DISABLE KEYS */;
INSERT INTO `equipment_requests` VALUES (6,28,'2016-12-16','event','P',NULL,'',NULL,NULL,'2016-12-12 06:25:11','2016-12-12 06:25:11'),(7,28,'2016-12-12','event','P',NULL,'',NULL,NULL,'2016-12-12 06:25:40','2016-12-12 06:25:40'),(8,28,'2016-12-12','Test1','C',NULL,'',NULL,NULL,'2016-12-12 08:29:11','2016-12-22 06:19:12');
/*!40000 ALTER TABLE `equipment_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_levels`
--

DROP TABLE IF EXISTS `job_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(4) DEFAULT NULL,
  `rank` varchar(30) DEFAULT NULL,
  `grade` varchar(2) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_levels`
--

LOCK TABLES `job_levels` WRITE;
/*!40000 ALTER TABLE `job_levels` DISABLE KEYS */;
INSERT INTO `job_levels` VALUES (12,'1','Staff','1','A','2016-08-08 16:17:10','2016-08-08 16:17:10');
/*!40000 ALTER TABLE `job_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_statuses`
--

DROP TABLE IF EXISTS `job_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_statuses`
--

LOCK TABLES `job_statuses` WRITE;
/*!40000 ALTER TABLE `job_statuses` DISABLE KEYS */;
INSERT INTO `job_statuses` VALUES (9,'Regular','A','2016-08-08 16:17:23','2016-08-08 16:17:23'),(10,'Probationary','A','2016-08-09 09:42:59','2016-08-09 09:42:59'),(11,'Casual','A','0000-00-00 00:00:00','2016-08-12 02:51:41'),(12,'Resigned','A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'Terminated','A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'Student Trainee','A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'Consultant','A','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `job_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_titles`
--

DROP TABLE IF EXISTS `job_titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_titles`
--

LOCK TABLES `job_titles` WRITE;
/*!40000 ALTER TABLE `job_titles` DISABLE KEYS */;
INSERT INTO `job_titles` VALUES (6,'Software Developer','Software Developer','A','2016-08-08 16:15:49','2016-08-08 16:15:49'),(7,'Software Solutions Analyst',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,'Quality Assurance Analyst',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,'Technical Writer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,'Product Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'Training Assistant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'Training Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'Software Solutions Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'Sales Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'Finance Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,'Account Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,'Marketing/ Hardware Associate',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,'Jr. Marketing/Hardware Specialist',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,'Sr. Marketing/Hardware Specialist',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'Sales Admin',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'Accounting Assistant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'Software Solutions Associate',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,'Sr. Project Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'Implementation Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,'Programmer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,'Sr. Software Engineer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,'Software Development Supervisor',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,'Chief Technology Officer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,'QA Specialist',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,'Chief Operating Officer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,'CEO/ President',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,'HR and Admin Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,'Administrative Assistant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,'Business Development Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,'Software Development Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'OMD Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,'Software Solutions Supervisor',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,'Marketing Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,'Jr. Project Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,'Director for Operations',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(41,'Project Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(42,'Graphic Artist',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(43,'Merchant Activation Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(44,'Telemarketer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(45,'Messenger',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(46,'Apprentice Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(47,'Support Apprentice',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(48,'Software Engineer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(49,'HR Officer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(50,'Sr. Developer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(51,'Jr. Project Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(52,'Managing Director',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(53,'Jr. FICO Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(54,'Sr. FICO Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(55,'Sr. SD Consultant/ABAP',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(56,'Jr. SD Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(57,'Sr. MM Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(58,'Sr. FICO Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(59,'Executive Assistant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(60,'General Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(61,'Retail Lab Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(62,'Accounting Analyst',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(63,'HR and Admin Assistant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(64,'Liaison Officer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(65,'Driver',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(66,'Project Consultant',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(67,'Content Writer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(68,'Business Analyst',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(69,'Jr. Account Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(70,'Graphic Designer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(71,'Director of Transformation',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(72,'Accounting Specialist',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(73,'Sr. Account Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(74,'Chief Finance Officer','','A','0000-00-00 00:00:00','2016-12-19 03:55:19'),(75,'Training Associate',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(76,'Software Developer',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(77,'Product Director',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(78,'QA Tester',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00'),(79,'Operations Manager',NULL,'A','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `job_titles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaves`
--

DROP TABLE IF EXISTS `leaves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `no_of_days` float DEFAULT NULL,
  `leave_type` varchar(30) DEFAULT NULL,
  `reason` varchar(250) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `reviewed_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `credits_used` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_leaves_on_employee_id` (`employee_id`),
  KEY `reviewed_by` (`reviewed_by`),
  CONSTRAINT `leaves_ibfk_1` FOREIGN KEY (`reviewed_by`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=315 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaves`
--

LOCK TABLES `leaves` WRITE;
/*!40000 ALTER TABLE `leaves` DISABLE KEYS */;
INSERT INTO `leaves` VALUES (293,17,'2016-09-26','2016-09-29',4,'Vacation Leave','Vacation','A','',18,'2016-09-01 08:07:07','2016-09-01 08:08:15',4),(294,20,'2016-09-26','2016-09-26',1,'Sick Leave','Fever','A','',18,'2016-09-27 08:11:34','2016-10-10 06:43:29',1),(295,17,'2016-10-06','2016-10-06',1,'Sick Leave','Headache','A','',18,'2016-10-06 08:35:01','2016-10-10 06:42:50',1),(296,19,'2016-10-10','2016-10-10',0.5,'Emergency Leave','Personal Errands','A','This is approved in the old ems',18,'2016-10-20 06:16:30','2016-10-20 08:21:42',0),(297,28,'2016-11-16','2016-11-18',3,'Sick Leave','lelele','C','',NULL,'2016-11-17 03:10:25','2016-11-17 03:10:34',0),(298,28,'2016-11-18','2016-11-22',4.5,'Maternity/Paternity Leave','yes','A','',28,'2016-11-18 01:16:07','2016-11-18 01:17:00',0),(299,28,'2016-11-16','2016-11-16',0.5,'Sick Leave','sick','C','',NULL,'2016-11-18 08:29:05','2016-11-18 08:32:04',0),(300,20,'2016-12-19','2016-12-23',5,'Sick Leave','Out of the Country - Christmas Vacation','C','',NULL,'2016-11-23 06:51:25','2016-11-23 06:54:18',0),(301,20,'2016-12-26','2016-12-26',1,'Sick Leave','Out of the Country - Christmas Vacation','C','',NULL,'2016-11-23 06:52:05','2016-11-23 06:54:21',0),(302,20,'2016-12-27','2016-12-29',3,'Vacation Leave','Out of the Country - Christmas Vacation','C','',NULL,'2016-11-23 06:52:59','2016-11-23 06:54:23',0),(303,20,'2016-12-19','2016-12-23',5,'Sick Leave','Christmas Leave','A','',15,'2016-11-23 06:55:00','2016-12-08 03:42:37',5),(304,20,'2016-12-26','2016-12-26',1,'Sick Leave','Christmas Leave','A','',15,'2016-11-23 06:55:18','2016-12-08 03:42:29',1),(305,20,'2016-12-27','2016-12-29',3,'Vacation Leave','Christmas Leave','A','',15,'2016-11-23 06:55:38','2016-12-08 03:42:29',3),(306,20,'2017-01-02','2017-01-05',4,'Sick Leave','Christmas Leave','C','',NULL,'2016-11-23 06:55:59','2016-12-05 10:38:40',0),(307,20,'2017-01-06','2017-01-06',1,'Sick Leave w/o Pay','Christmas Leave','A','',15,'2016-11-23 06:56:40','2016-12-08 03:03:48',0),(308,28,'2016-11-24','2016-11-24',1,'Sick Leave','Fever','C','',28,'2016-11-25 07:33:47','2016-12-12 02:50:55',0),(309,28,'2016-11-01','2016-11-04',3.5,'Sick Leave','3.5 days','C','',28,'2016-11-25 08:15:17','2016-12-12 02:51:01',0),(310,28,'2016-12-19','2016-12-23',5,'Vacation Leave','boracay!','D','',28,'2016-11-25 08:29:07','2016-12-12 02:46:38',0),(311,28,'2016-11-07','2016-11-07',1,'Birthday Leave','birthday leave','C','',NULL,'2016-11-28 01:22:48','2016-11-28 01:22:53',0),(312,20,'2017-01-02','2017-01-05',4,'Vacation Leave','Christmas Leave','A','',15,'2016-12-05 10:39:10','2016-12-08 03:42:29',4),(313,28,'2016-12-13','2016-12-13',1,'Sick Leave','Test1 - Sick','A','',28,'2016-12-12 02:47:44','2016-12-12 02:48:09',0),(314,28,'2016-12-14','2016-12-14',1,'Vacation Leave','VL - 3','D','',28,'2016-12-12 02:50:17','2016-12-12 06:32:21',0);
/*!40000 ALTER TABLE `leaves` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `official_businesses`
--

DROP TABLE IF EXISTS `official_businesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `official_businesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `purpose` varchar(250) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `reviewed_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `client` varchar(50) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reviewed_by` (`reviewed_by`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `official_businesses_ibfk_1` FOREIGN KEY (`reviewed_by`) REFERENCES `employees` (`id`),
  CONSTRAINT `official_businesses_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `official_businesses`
--

LOCK TABLES `official_businesses` WRITE;
/*!40000 ALTER TABLE `official_businesses` DISABLE KEYS */;
INSERT INTO `official_businesses` VALUES (12,'2016-08-12 14:30:00','2016-08-12 18:30:00','UBEY','','A',18,'2016-08-16 03:49:19','2016-08-16 05:29:41','R&D',17),(13,'2016-08-15 08:30:00','2016-08-15 18:30:00','Work remotely','','A',18,'2016-08-16 03:51:35','2016-08-16 05:29:41','R&D',17),(14,'2016-08-12 14:00:00','2016-08-12 17:30:00','Team Planning','','A',18,'2016-08-16 05:51:58','2016-08-16 05:53:03','INTERNAL',20),(15,'2016-08-12 00:00:00','2016-08-12 23:54:00','Planning at Tagaytay','','A',23,'2016-08-18 03:03:20','2016-08-18 03:22:29','Internal - Enterprise and CX',25),(16,'2016-08-18 08:30:00','2016-08-18 14:00:00','Work from home in the morning','','A',18,'2016-08-18 07:35:08','2016-08-24 02:59:30','Internal',20),(17,'2016-08-19 08:30:00','2016-08-19 13:30:00','Work at dorm :)','','A',18,'2016-08-23 03:38:44','2016-08-23 03:39:48','R&D',17),(18,'2016-08-23 14:00:00','2016-08-23 20:00:00','Client turnover meeting and initial project scoping ','','A',18,'2016-08-24 02:25:34','2016-08-24 02:58:37','Abigail Farms',20),(19,'2016-09-16 08:30:00','2016-09-16 15:00:00','LGC Alignment Meeting at Binondo','','A',18,'2016-09-20 02:41:33','2016-09-22 05:42:09','LGC',20),(20,'2016-09-22 08:30:00','2016-09-22 18:30:00','On-site requirements gathering and meetings','','A',18,'2016-09-22 01:06:03','2016-09-22 05:41:19','Abigail Farms',20),(21,'2016-09-21 08:30:00','2016-09-21 18:30:00','On-site requirements gathering and meetings','','A',18,'2016-09-22 01:06:03','2016-09-22 05:41:22','Abigail Farms',20),(22,'2016-09-23 08:30:00','2016-09-23 18:30:00','Requirements gathering and onsite client meetings','','C',18,'2016-09-23 01:18:01','2016-09-27 08:14:11','Abigail Farms',20),(23,'2016-09-24 00:30:00','2016-09-24 12:31:00','Client meetings, requirements gathering, and travel back to Manila','','C',20,'2016-09-27 08:12:49','2016-09-27 08:13:12','AFarms',20),(24,'2016-09-23 00:00:00','2016-09-23 23:54:00','Client meetings, requirements gathering, travel to Manila','','A',18,'2016-09-27 08:14:16','2016-10-10 06:43:29','AFSI',20),(25,'2016-09-30 09:30:00','2016-09-30 18:30:00','Work Remotely','','A',18,'2016-10-06 08:34:13','2016-10-10 06:43:29','R&D',17),(26,'2016-10-07 12:00:00','2016-10-07 17:30:00','Northstar Warehouse Meeting','','A',18,'2016-10-11 00:49:06','2016-10-11 05:00:49','Northstar',20),(27,'2016-10-10 08:30:00','2016-10-10 18:30:00','Mobile application version update','','A',18,'2016-10-11 00:50:19','2016-10-11 05:01:10','Dizon Farms',20),(28,'2016-10-19 10:00:00','2016-10-19 19:00:00','Dizon Farms UAT Day 1','','A',18,'2016-10-20 01:31:51','2016-10-20 08:21:51','Dizon Farms',20),(29,'2016-10-19 00:00:00','2016-10-19 23:54:00','Work Remotely','','A',18,'2016-10-20 03:22:14','2016-10-20 08:20:09','R&D',17),(30,'2016-10-13 08:30:00','2016-10-13 14:00:00','Monthly Alignment Meeting','This is approved in the old ems portal','A',18,'2016-10-20 06:12:24','2016-10-20 08:21:51','LGC',19),(31,'2016-11-04 08:30:00','2016-11-04 17:30:00','Work Remotely','','A',18,'2016-11-04 03:12:05','2016-11-04 05:20:55','R&D',17),(32,'2016-11-11 08:30:00','2016-11-11 18:30:00','Dizon Farms Phase II Go-Live (Time in and out in Biometrics)','','A',18,'2016-11-15 05:21:55','2016-11-18 01:25:13','Dizon Farms',20),(33,'2016-11-08 00:00:00','2016-11-08 18:30:00','Dizon Farms Phase II Go-Live (Time in and out in Biometrics)','','A',18,'2016-11-15 05:21:55','2016-11-18 01:25:13','Dizon Farms',20),(34,'2016-11-09 08:30:00','2016-11-09 18:30:00','Dizon Farms Phase II Go-Live (Time in and out in Biometrics)','','A',18,'2016-11-15 05:21:55','2016-11-18 01:25:13','Dizon Farms',20),(35,'2016-11-10 08:30:00','2016-11-10 18:30:00','Dizon Farms Phase II Go-Live (Time in and out in Biometrics)','','A',18,'2016-11-15 05:21:55','2016-11-18 01:25:13','Dizon Farms',20),(36,'2016-11-12 08:30:00','2016-11-12 18:30:00','Dizon Farms Phase II Go-Live (Time in and out in Biometrics)','','A',18,'2016-11-15 05:21:57','2016-11-18 01:25:13','Dizon Farms',20),(37,'2016-11-14 09:00:00','2016-11-14 13:00:00','Work at home','','A',18,'2016-11-18 01:36:27','2016-11-18 04:06:08','Work at home',19),(38,'2016-11-21 08:00:00','2016-11-21 19:30:00','LGC Retail Conference 2016','Worked at the office after','A',18,'2016-11-23 02:20:13','2016-11-23 02:22:28','LGC',19),(39,'2016-11-29 00:00:00','2016-11-29 00:00:00','Site Visit','','C',NULL,'2016-11-29 07:01:45','2016-11-29 07:02:19','Choie',28),(40,'2016-12-06 08:30:00','2016-12-06 18:30:00','Dizon Farms Phase II Client Visit','','A',15,'2016-12-07 01:25:10','2016-12-08 03:42:29','Dizon Farms',20),(41,'2016-12-05 08:30:00','2016-12-05 18:30:00','JFC UAT','','A',15,'2016-12-07 06:49:35','2016-12-08 03:42:29','JFC',17),(42,'2016-12-07 08:30:00','2016-12-07 18:30:00','JFC UAT','','A',15,'2016-12-07 06:49:35','2016-12-08 03:42:29','JFC',17),(43,'2016-12-06 08:30:00','2016-12-06 18:30:00','JFC UAT','','A',15,'2016-12-07 06:49:35','2016-12-08 03:41:54','JFC',17),(44,'2016-12-12 08:30:00','2016-12-12 18:30:00','UAT ','','A',15,'2016-12-07 07:22:03','2016-12-08 03:41:54','JFC',19),(45,'2016-12-08 08:30:00','2016-12-08 18:30:00','UAT ','','A',15,'2016-12-07 07:22:03','2016-12-08 03:41:53','JFC',19),(46,'2016-12-06 08:30:00','2016-12-06 18:30:00','UAT ','','A',15,'2016-12-07 07:22:03','2016-12-08 03:16:02','JFC',19),(47,'2016-12-05 08:30:00','2016-12-05 18:30:00','UAT ','','A',15,'2016-12-07 07:22:04','2016-12-08 03:16:02','JFC',19),(48,'2016-12-09 08:30:00','2016-12-09 17:30:00','UAT ','','A',15,'2016-12-07 07:22:04','2016-12-08 03:16:02','JFC',19),(49,'2016-12-07 08:30:00','2016-12-07 18:30:00','UAT ','','A',15,'2016-12-07 07:22:04','2016-12-08 03:16:02','JFC',19),(50,'2016-12-14 08:30:00','2016-12-14 18:30:00','UAT ','','A',15,'2016-12-07 07:22:05','2016-12-08 03:04:05','JFC',19),(51,'2016-12-15 08:30:00','2016-12-15 18:30:00','UAT ','','A',15,'2016-12-07 07:22:05','2016-12-08 03:04:05','JFC',19),(52,'2016-12-13 08:30:00','2016-12-13 18:30:00','UAT ','','A',15,'2016-12-07 07:22:05','2016-12-08 03:04:05','JFC',19),(53,'2016-12-01 10:00:00','2016-12-01 21:30:00','Missed log in','','P',NULL,'2016-12-08 06:07:57','2016-12-08 06:07:57','None',19),(54,'2016-12-12 08:30:00','2016-12-12 08:31:00','Client visit\n','','P',NULL,'2016-12-12 03:06:39','2016-12-12 03:06:39','Coca-cola',28),(55,'2016-12-12 00:00:00','2016-12-12 23:54:00','Client visit\n','','P',NULL,'2016-12-12 03:06:39','2016-12-12 03:06:39','Coca-cola',28),(56,'2016-12-11 00:00:00','2016-12-11 23:54:00','test','','P',NULL,'2016-12-12 03:12:27','2016-12-12 03:12:27','test1',28),(57,'2016-12-05 08:30:00','2016-12-05 23:54:00','test','','P',NULL,'2016-12-12 03:43:59','2016-12-12 03:43:59','choie',28),(58,'2016-12-16 08:30:00','2016-12-16 18:30:00','Alignment meeting','','P',NULL,'2016-12-19 06:20:12','2016-12-19 06:20:12','LGC',19),(59,'2016-12-22 00:00:00','2016-12-22 00:00:00','Same date','','C',NULL,'2016-12-22 06:14:58','2016-12-22 06:19:20','Test2',28),(60,'2016-12-22 08:30:00','2016-12-22 08:31:00','Same date','','P',NULL,'2016-12-22 06:14:58','2016-12-22 06:14:58','Test2',28);
/*!40000 ALTER TABLE `official_businesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offset_overtimes`
--

DROP TABLE IF EXISTS `offset_overtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offset_overtimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `overtime_id` int(11) DEFAULT NULL,
  `offset_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hours_used` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_offset_overtimes_on_overtime_id` (`overtime_id`),
  KEY `index_offset_overtimes_on_offset_id` (`offset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offset_overtimes`
--

LOCK TABLES `offset_overtimes` WRITE;
/*!40000 ALTER TABLE `offset_overtimes` DISABLE KEYS */;
INSERT INTO `offset_overtimes` VALUES (5,133,3,'2016-11-28 08:37:00','2016-11-28 08:37:00',1),(6,134,3,'2016-11-28 08:37:01','2016-11-28 08:37:01',2),(7,133,4,'2016-11-28 09:04:50','2016-11-28 09:04:50',1),(8,134,4,'2016-11-28 09:04:50','2016-11-28 09:04:50',2),(9,134,5,'2016-11-29 05:27:15','2016-11-29 05:27:15',3),(10,133,5,'2016-11-29 05:27:15','2016-11-29 05:27:15',1),(11,136,6,'2016-11-29 05:30:40','2016-11-29 05:30:40',2),(12,135,6,'2016-11-29 05:30:40','2016-11-29 05:30:40',2),(13,135,7,'2016-11-29 06:57:16','2016-11-29 06:57:16',4);
/*!40000 ALTER TABLE `offset_overtimes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offsets`
--

DROP TABLE IF EXISTS `offsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `offset_type` varchar(30) DEFAULT NULL,
  `purpose` varchar(250) DEFAULT NULL,
  `no_of_hours` int(11) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `reviewed_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reviewed_by` (`reviewed_by`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `offsets_ibfk_1` FOREIGN KEY (`reviewed_by`) REFERENCES `employees` (`id`),
  CONSTRAINT `offsets_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offsets`
--

LOCK TABLES `offsets` WRITE;
/*!40000 ALTER TABLE `offsets` DISABLE KEYS */;
INSERT INTO `offsets` VALUES (3,'2016-11-28',28,'Half Day PM','Offset',4,'','C',NULL,'2016-11-28 08:37:00','2016-11-28 08:40:20'),(4,'2016-11-22',28,'Half Day AM','halfday',4,'','C',NULL,'2016-11-28 09:04:50','2016-11-29 03:24:18'),(5,'2016-12-01',28,'Half Day AM','Test',4,'','D',28,'2016-11-29 05:27:15','2016-12-12 02:46:38'),(6,'2016-12-07',28,'Half Day PM','Vacation',4,'','C',NULL,'2016-11-29 05:30:40','2016-11-29 06:54:41'),(7,'2016-12-07',28,'Half Day AM','Testing, without pending OT',4,'','C',NULL,'2016-11-29 06:57:16','2016-11-29 06:58:27');
/*!40000 ALTER TABLE `offsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `overtimes`
--

DROP TABLE IF EXISTS `overtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `overtimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `no_of_hours` float DEFAULT NULL,
  `calendar_classification` tinyint(4) DEFAULT NULL,
  `expected_output` varchar(250) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `reviewed_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_overtimes_on_employee_id` (`employee_id`),
  KEY `reviewed_by` (`reviewed_by`),
  CONSTRAINT `overtimes_ibfk_1` FOREIGN KEY (`reviewed_by`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `overtimes`
--

LOCK TABLES `overtimes` WRITE;
/*!40000 ALTER TABLE `overtimes` DISABLE KEYS */;
INSERT INTO `overtimes` VALUES (73,17,'2016-08-08 20:00:00','2016-08-08 23:55:00',4.91667,1,'IEMS Deployment','A',' **Overtime til 1AM of the next day**',18,'2016-08-16 03:47:51','2016-08-16 05:28:48'),(74,17,'2016-08-16 20:00:00','2016-08-16 23:55:00',3.91667,1,'Development for Rebisco (Security)','A','',18,'2016-08-17 01:49:41','2016-08-26 03:31:49'),(75,14,'2016-08-08 20:00:00','2016-08-08 23:55:00',4.91667,1,'IEMS deployment','A',' **Overtime til 1AM of the next day**',15,'2016-08-17 07:37:27','2016-08-18 02:15:02'),(76,25,'2016-08-17 21:00:00','2016-08-17 23:55:00',2.91667,1,'-IRC regression testing Merging for NTE, DIZON\r\n','A','',23,'2016-08-18 03:06:24','2016-08-18 03:21:47'),(77,25,'2016-08-18 00:00:00','2016-08-18 02:00:00',2,1,'- IRC regression testing, merge NTE, Dizon ','A','',23,'2016-08-18 03:07:30','2016-08-18 03:22:04'),(78,17,'2016-08-17 20:00:00','2016-08-17 23:55:00',4.91667,1,'IRC development ( hourly sales downloadable )','A',' **Overtime til 1AM of the next day**',18,'2016-08-18 08:36:34','2016-08-26 03:31:49'),(79,17,'2016-08-22 20:00:00','2016-08-22 23:55:00',3.91667,1,'Development for IRC - Sub category','A','',18,'2016-08-23 03:40:54','2016-08-24 02:59:49'),(80,20,'2016-08-25 18:30:00','2016-08-25 23:00:00',4.5,1,'1. Rebisco application testing and release\r\n2. Feedback to Rebsico issues\r\n3. Communications','A','',18,'2016-08-26 00:12:13','2016-08-26 03:31:37'),(81,20,'2016-08-31 18:30:00','2016-08-31 22:50:00',4.33333,1,'Abigail Farms mapping and integration document','A','Also filed in old iEMS',18,'2016-09-05 04:06:07','2016-09-07 01:42:58'),(82,17,'2016-09-05 20:00:00','2016-09-05 22:30:00',2.5,1,'Northstar Migration from PLDT to Rackspace','A','',18,'2016-09-07 04:03:23','2016-09-07 04:05:24'),(83,17,'2016-09-06 19:00:00','2016-09-06 23:55:00',4.91667,1,'Support for NTE UAT','A','',18,'2016-09-07 04:04:29','2016-09-07 04:05:31'),(84,20,'2016-09-05 18:30:00','2016-09-05 22:30:00',4,1,'Northstar migration testing\r\nRebisco testing','C','',NULL,'2016-09-07 05:39:49','2016-09-07 05:39:58'),(85,20,'2016-09-05 18:30:00','2016-09-05 22:30:00',4,1,'Northstar migration testing\r\nRebisco testing','A','Filed in Old iEMS also',18,'2016-09-07 05:40:47','2016-09-08 02:05:14'),(86,20,'2016-09-06 18:30:00','2016-09-06 22:45:00',4.25,1,'Rebisco Testing (before Technical Checkpoint Meeting)\r\nDizon Farms Testing\r\n','A','Also filed in Old iEMS ',18,'2016-09-07 05:42:15','2016-09-08 02:05:14'),(87,20,'2016-10-11 18:30:00','2016-10-11 23:30:00',5,1,'1. Abigail Farms Mobile Specs, 2. RBC App Testing, 3. Documents for communications to client','A','',18,'2016-10-11 15:30:45','2016-10-13 01:38:51'),(88,20,'2016-10-12 18:30:00','2016-10-12 23:55:00',6.41667,1,'1. RBC fixes for application release (Refresh pricing function)','A',' **Overtime til 1AM of the next day**',18,'2016-10-13 01:40:39','2016-10-20 08:23:38'),(89,20,'2016-10-13 18:30:00','2016-10-13 23:15:00',4.75,1,'1. Dizon server maintenance and application testing, 2. Dizon EUT Exam, RBC Testing','A','',18,'2016-10-15 06:47:33','2016-10-20 08:23:38'),(90,20,'2016-10-18 18:30:00','2016-10-18 22:45:00',4.25,1,'1. Dizon Support, 2. Dizon Phase II UAT and EUT preparations','C','',NULL,'2016-10-18 01:07:29','2016-10-20 01:01:35'),(91,20,'2016-10-17 18:30:00','2016-10-17 22:45:00',4.25,1,'1. Dizon Support, 2. Dizon Phase II UAT and EUT preparations','A','',18,'2016-10-20 01:01:24','2016-10-20 08:22:55'),(92,20,'2016-10-18 18:30:00','2016-10-18 22:30:00',4,1,'1. Dizon UAT and EUT Preparations','A','',18,'2016-10-20 01:02:03','2016-10-20 08:22:55'),(93,17,'2016-10-13 19:30:00','2016-10-13 23:00:00',3.5,1,'Installation og DIZON A3 and A4, Deployment of IRC code to DIZON','A','',18,'2016-10-20 03:21:50','2016-10-20 08:22:07'),(94,19,'2016-10-08 08:30:00','2016-10-08 18:30:00',10,2,'JFC Meeting in Cebu','A','',18,'2016-10-20 06:01:11','2016-10-20 08:22:55'),(95,19,'2016-10-12 18:30:00','2016-10-12 23:00:00',4.5,1,'Preparation for LGC Meeting','A','',18,'2016-10-20 06:02:26','2016-10-20 08:23:38'),(96,19,'2016-10-13 18:30:00','2016-10-13 21:00:00',2.5,1,'Run through iRc Support (Zendesk)','A','',18,'2016-10-20 06:03:39','2016-10-20 08:23:38'),(97,19,'2016-10-17 18:30:00','2016-10-17 22:00:00',3.5,1,'LGC Support Stats + marketing plan presentation (start)','A','',18,'2016-10-20 06:06:26','2016-10-20 08:23:38'),(98,19,'2016-10-18 18:30:00','2016-10-18 21:00:00',2.5,1,'Business Process Diagrams','A','',18,'2016-10-20 06:08:03','2016-10-20 08:23:38'),(99,19,'2016-10-20 18:30:00','2016-10-20 23:30:00',5,1,'LGC Christmas Promo, LGC MySQL tuning, JFC test','A','',18,'2016-10-21 02:12:17','2016-11-04 05:20:55'),(100,19,'2016-10-27 18:30:00','2016-10-27 21:30:00',3,1,'Pentstar Meeting','A','',18,'2016-11-03 23:49:37','2016-11-04 05:20:55'),(101,19,'2016-10-26 18:30:00','2016-10-26 23:12:00',4.7,1,'Zendesk Seminar + Preparation for Pentstar Meeting','A','',18,'2016-11-03 23:50:47','2016-11-04 05:20:55'),(102,19,'2016-10-24 18:30:00','2016-10-24 20:34:00',2.06667,1,'Presentation and Stats for Pentstar meeting','A','',18,'2016-11-03 23:52:57','2016-11-04 05:20:55'),(103,19,'2016-10-21 17:30:00','2016-10-21 21:05:00',3.58333,1,'Preparation for JFC UI/UX Training','A','',18,'2016-11-03 23:55:00','2016-11-04 05:20:55'),(104,17,'2016-10-24 20:30:00','2016-10-24 23:30:00',3,1,'DIZON SERVER FIX','A','',18,'2016-11-04 03:08:29','2016-11-04 05:16:49'),(105,17,'2016-10-25 20:30:00','2016-10-25 23:45:00',3.25,1,'DIZON SERVER FIX','A','',18,'2016-11-04 03:09:08','2016-11-04 05:20:55'),(106,20,'2016-11-07 18:30:00','2016-11-07 21:30:00',3,1,'Dizon Farms Phase II Go-Live Preparation','A','',18,'2016-11-15 05:23:27','2016-11-18 01:25:13'),(107,20,'2016-11-09 04:00:00','2016-11-09 06:00:00',2,1,'Dizon Farms Phase II Go-Live Proper','A','',18,'2016-11-15 05:24:04','2016-11-18 01:25:13'),(108,20,'2016-11-10 03:00:00','2016-11-10 05:30:00',2.5,1,'Dizon Farms Phase II Go-Live Proper','A','',18,'2016-11-15 05:25:43','2016-11-18 01:25:13'),(109,20,'2016-11-11 03:00:00','2016-11-11 07:30:00',4.5,1,'Dizon Farms Phase II Go-Live Proper','A','',18,'2016-11-15 05:26:47','2016-11-18 01:25:13'),(110,20,'2016-11-11 17:00:00','2016-11-11 23:55:00',6.91667,1,'Dizon Farms Phase II Go-Live Proper','A','',18,'2016-11-15 05:39:13','2016-11-18 01:25:13'),(111,20,'2016-11-12 00:00:00','2016-11-12 13:00:00',13,2,'Dizon Farms Phase II Go-Live Proper','A','',18,'2016-11-15 05:40:17','2016-11-18 01:25:13'),(112,19,'2016-11-16 18:30:00','2016-11-16 22:30:00',4,1,'Receipt for JFC, Review JFC Test case, Products for LGC Chritsmas Promo\r\n','A','',18,'2016-11-18 01:10:17','2016-11-18 01:14:48'),(113,19,'2016-11-11 17:30:00','2016-11-11 23:45:00',6.25,1,'JFC Use Cases','A','',18,'2016-11-18 01:11:21','2016-11-18 01:25:13'),(114,19,'2016-11-09 18:30:00','2016-11-09 22:00:00',3.5,1,'LGC Status','A','',18,'2016-11-18 01:13:51','2016-11-18 01:25:13'),(115,19,'2016-11-10 18:30:00','2016-11-10 21:30:00',3,1,'Preparation for LGC alignment meeting + started JFC Use Cases','A','',18,'2016-11-18 01:18:14','2016-11-18 01:25:23'),(116,19,'2016-11-18 17:30:00','2016-11-15 21:20:00',-68.1667,1,'Manage sprints?','C','',18,'2016-11-18 01:31:52','2016-11-23 05:39:24'),(117,17,'2016-11-10 01:00:00','2016-11-10 07:00:00',6,1,'DIZON Live Support','A','Received a call on around 1 AM from Ace to help them with Dizon Phase II Live',18,'2016-11-18 02:34:52','2016-11-18 04:05:34'),(118,17,'2016-11-10 20:00:00','2016-11-10 23:55:00',3.91667,1,'DIZON Live Phase II Support Day 2','A','',18,'2016-11-18 02:37:21','2016-11-18 04:05:27'),(119,17,'2016-11-09 01:00:00','2016-11-09 07:00:00',6,1,'DIZON Emergency Support - Support for DIZON PHASE II LIVE','A','',18,'2016-11-18 04:27:30','2016-11-18 09:47:20'),(120,17,'2016-11-09 20:30:00','2016-11-09 23:55:00',3.41667,1,'DIZON PHASE II SUPPORT DAY 2','A','',18,'2016-11-18 04:28:19','2016-11-18 09:47:20'),(121,17,'2016-11-10 17:30:00','2016-11-10 19:30:00',2,1,'DIZON PHASE II LIVE SUPPORT - DAY2','A','',18,'2016-11-18 04:29:43','2016-11-18 09:47:20'),(122,28,'2016-11-17 17:30:00','2016-11-17 19:30:00',2,1,'1. Report some bugs.\r\n2. Create test scripts.','C','',NULL,'2016-11-18 09:05:21','2016-11-18 09:05:54'),(123,28,'2016-11-17 17:30:00','2016-11-17 19:30:00',2,1,'create test script','C','',NULL,'2016-11-18 09:06:36','2016-11-18 09:06:46'),(124,28,'2016-11-18 17:30:00','2016-11-18 23:30:00',6,1,'Creating some test scripts','D',' **Overtime til 1AM of the next day**',28,'2016-11-18 09:11:29','2016-12-12 02:46:38'),(125,28,'2016-11-17 17:30:00','2016-11-17 23:55:00',7.41667,1,'test scripts','D',' **Overtime til 1AM of the next day** **Overtime til 1AM of the next day**',28,'2016-11-18 09:14:11','2016-12-12 02:46:38'),(126,28,'2016-11-14 17:30:00','2016-11-14 19:54:00',3.4,1,'scripts.\r\nbugs.','C',' **Overtime til 1AM of the next day** **Overtime til 1AM of the next day**',NULL,'2016-11-18 09:15:55','2016-11-18 09:16:06'),(127,28,'2016-11-14 17:30:00','2016-11-14 23:54:00',7.4,1,'test script\r\nbugs ','D',' **Overtime til 1AM of the next day**',28,'2016-11-18 09:16:50','2016-12-12 02:46:38'),(128,28,'2016-11-21 18:30:00','2016-11-26 23:30:00',125,1,'deadline','C','',NULL,'2016-11-21 00:42:48','2016-11-21 00:43:23'),(129,19,'2016-11-23 18:30:00','2016-11-15 21:30:00',-189,1,'Managing sprints\r\n\r\nRe-application: the last OT applied is erroneous. ','C','Start date is Nov 18 and end date is Nov 15. I\'ve already informed Ms. Dada and Ms. Barbs about this',NULL,'2016-11-23 05:42:16','2016-12-07 06:28:29'),(130,28,'2016-11-28 18:30:00','2016-11-28 20:30:00',2,1,'deployment\r\n','C','',NULL,'2016-11-28 05:32:18','2016-11-28 05:34:43'),(131,28,'2016-11-28 18:30:00','2016-11-28 00:00:00',-18.5,1,'Deployment','C','',NULL,'2016-11-28 05:35:05','2016-11-28 05:38:25'),(132,28,'2016-11-28 18:30:00','2016-11-28 00:00:00',-18.5,1,'Deployment','C','',NULL,'2016-11-28 05:56:25','2016-11-28 05:56:41'),(133,28,'2016-11-28 18:30:00','2016-11-28 20:00:00',1.5,1,'deplyoment','C','',NULL,'2016-11-28 05:57:15','2016-12-22 06:19:36'),(134,28,'2016-11-29 18:30:00','2016-11-29 21:30:00',3,1,'Deployment','P','',NULL,'2016-11-28 05:57:54','2016-12-12 02:46:38'),(135,28,'2016-12-02 18:30:00','2016-12-02 22:30:00',4,1,'Testing and stuffs','D','',28,'2016-11-29 05:28:17','2016-12-12 02:46:38'),(136,28,'2016-11-24 18:30:00','2016-11-24 20:30:00',2,1,'Testing\r\n','D','',28,'2016-11-29 05:29:09','2016-12-12 02:46:38'),(137,20,'2016-12-01 20:00:00','2016-12-01 23:55:00',3.91667,1,'Dizon Phase II site visit and monitoring','A','',15,'2016-12-02 07:27:44','2016-12-08 03:42:29'),(138,20,'2016-12-02 00:00:00','2016-12-02 08:00:00',8,1,'Dizon Phase II site visit and monitoring','A','',15,'2016-12-02 07:28:13','2016-12-08 03:42:29'),(139,19,'2016-11-15 18:30:00','2016-11-15 21:30:00',3,1,'Re-application: the last OT applied is erroneous. ','A','',15,'2016-12-07 06:29:24','2016-12-08 03:42:29'),(140,19,'2016-11-18 18:30:00','2016-11-18 21:45:00',3.25,1,'FC Advance / Partial Payment use case ','A','',15,'2016-12-07 06:34:31','2016-12-08 03:42:29'),(141,19,'2016-12-07 18:30:00','2016-11-21 21:00:00',-381.5,1,'Manage support tickets','C','',NULL,'2016-12-07 06:37:24','2016-12-07 06:37:30'),(142,19,'2016-11-21 18:30:00','2016-11-21 21:30:00',3,1,'Manage support tickets','A','',15,'2016-12-07 06:38:57','2016-12-08 03:42:29'),(143,19,'2016-11-22 18:30:00','2016-11-22 20:30:00',2,1,'JFC Test case for Discount computation','A','',15,'2016-12-07 06:43:00','2016-12-08 03:42:29'),(144,17,'2016-11-29 19:30:00','2016-11-29 23:55:00',4.41667,1,'Export BI Data for JFC','A','',15,'2016-12-07 06:45:48','2016-12-08 03:42:29'),(145,17,'2016-11-30 00:00:00','2016-11-30 04:00:00',4,1,'Export BI Data for JFC','A','',15,'2016-12-07 06:47:16','2016-12-08 03:42:29'),(146,19,'2016-11-25 18:30:00','2016-11-25 20:30:00',2,1,'IRC Test','C','',NULL,'2016-12-07 06:52:25','2016-12-07 07:09:20'),(147,19,'2016-11-28 18:30:00','2016-11-28 21:30:00',3,1,'IRC Test + UAT doc draft','A','',15,'2016-12-07 06:53:24','2016-12-08 03:41:54'),(148,19,'2016-11-29 18:30:00','2016-11-29 21:35:00',3.08333,1,'Manage LGC Christmas Promo','A','',15,'2016-12-07 06:58:17','2016-12-08 03:41:54'),(149,19,'2016-11-30 10:30:00','2016-11-30 21:30:00',11,1,'Manage LGC Christmas promo and visit client in bulacan','A','',15,'2016-12-07 07:01:29','2016-12-08 03:41:54'),(150,19,'2016-12-01 18:30:00','2016-12-01 21:30:00',3,1,'JFC - UAT Document','A','',15,'2016-12-07 07:03:56','2016-12-08 03:41:54'),(151,19,'2016-12-02 17:30:00','2016-12-02 20:30:00',3,1,'JFC - UAT Forms','A','',15,'2016-12-07 07:08:29','2016-12-08 03:41:54'),(152,19,'2016-12-03 13:00:00','2016-12-03 18:00:00',5,2,'UAT forms and testing','A','',15,'2016-12-07 07:15:17','2016-12-08 03:15:49'),(153,19,'2016-11-25 17:30:00','2016-11-25 19:30:00',2,1,'IRC test','C','',19,'2016-12-08 03:41:32','2016-12-08 03:42:20'),(154,19,'2016-11-25 17:30:00','2016-11-25 20:30:00',3,1,'IRC Test','C','',NULL,'2016-12-08 03:42:52','2016-12-08 03:44:27'),(155,19,'2016-11-23 18:30:00','2016-11-23 23:00:00',4.5,1,'Forgot to take note but I\'m probably doing tests and use cases','A','',18,'2016-12-08 03:59:36','2016-12-08 05:31:46'),(156,19,'2016-11-25 17:30:00','2016-11-25 23:15:00',5.75,1,'IRC Test','A','',18,'2016-12-08 04:06:35','2016-12-08 05:31:54');
/*!40000 ALTER TABLE `overtimes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `special_days`
--

DROP TABLE IF EXISTS `special_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `special_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `day_type` varchar(50) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `special_days`
--

LOCK TABLES `special_days` WRITE;
/*!40000 ALTER TABLE `special_days` DISABLE KEYS */;
INSERT INTO `special_days` VALUES (13,'2016-08-29','National Heroes Day','Regular Holiday','A','2016-09-07 08:49:21','2016-09-07 08:49:21');
/*!40000 ALTER TABLE `special_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `undertimes`
--

DROP TABLE IF EXISTS `undertimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `undertimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `nature` varchar(20) DEFAULT NULL,
  `reason` varchar(250) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reviewed_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_undertimes_on_employee_id` (`employee_id`),
  KEY `fk_reviewed_by` (`reviewed_by`),
  CONSTRAINT `fk_reviewed_by` FOREIGN KEY (`reviewed_by`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `undertimes`
--

LOCK TABLES `undertimes` WRITE;
/*!40000 ALTER TABLE `undertimes` DISABLE KEYS */;
INSERT INTO `undertimes` VALUES (17,28,'2016-11-18 16:30:00','Emergency','sick','C','','2016-11-18 08:31:05','2016-11-28 01:50:50',28),(18,28,'2016-11-21 16:30:00','Anticipated','requirements','C','','2016-11-18 08:32:48','2016-11-18 08:50:21',28),(19,28,'2016-11-21 16:30:00','Emergency','urgent ','C','','2016-11-18 08:50:51','2016-11-18 08:50:58',NULL),(20,28,'2016-11-28 12:30:00','Emergency','yes','C','','2016-11-28 02:26:30','2016-11-28 02:27:01',NULL),(21,28,'2016-11-28 15:30:00','Emergency','yes','C','','2016-11-28 02:34:34','2016-12-22 04:10:56',28);
/*!40000 ALTER TABLE `undertimes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `oauth_token` varchar(100) DEFAULT NULL,
  `oauth_expires_at` datetime DEFAULT NULL,
  `provider` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-22 15:55:40
