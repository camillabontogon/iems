file = File.open("_bu-approvers.in", "r").readlines.each do |line|
	# emp_num,SUM(l.no_of_days)
	emp_array = line.split(",")
	# puts emp_array

	@employees = Employee.where(business_unit_id: emp_array[0].to_i).all
	@approver = Employee.find_by(employee_no: emp_array[1].gsub(/\n/, ''))

	# Parameters: {"utf8"=>"√", "approver"=>{"approver_id"=>"13"}, "id"=>"2"}

	# @employee = Employee.find(params[:id])
 #  @approver = @employee.links_to_superior.build(params[:approver])
 #  if @approver.save
 #    redirect_to edit_employee_path(@employee)
 #  else
 #    render "new"
 #  end

	if @employees.count > 0
		@employees.each do |emp|
			Approver.create!(employee_id: emp.id, approver_id: @approver.id)
		end
	end

	# @emp = Employee.find_by(employee_no: emp_array[0])
	# result = @emp.sick_leave_credit - emp_array[1].to_i
	# Employee.find_by(employee_no: emp_array[0]).update(sick_leave_balance: result)

end