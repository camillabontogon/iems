file = File.open("_sl_balance.csv", "r").readlines.each do |line|
	# emp_num,SUM(l.no_of_days)
	emp_array = line.split(",")
	
	@emp = Employee.find_by(employee_no: emp_array[0])
	result = @emp.sick_leave_credit - emp_array[1].to_i
	Employee.find_by(employee_no: emp_array[0]).update(sick_leave_balance: result)

end