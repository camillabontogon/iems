# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170201085511) do

  create_table "approvers", force: :cascade do |t|
    t.integer  "employee_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "approver_id", limit: 4
  end

  add_index "approvers", ["approver_id"], name: "fk_approver_id", using: :btree
  add_index "approvers", ["employee_id"], name: "index_approvers_on_employees_id", using: :btree

  create_table "business_units", force: :cascade do |t|
    t.string   "name",       limit: 50
    t.string   "status",     limit: 1
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "company_infos", force: :cascade do |t|
    t.string   "name",       limit: 50
    t.string   "phone",      limit: 20
    t.string   "address",    limit: 200
    t.string   "zip",        limit: 20
    t.string   "tin",        limit: 20
    t.string   "pagibig",    limit: 20
    t.string   "philhealth", limit: 20
    t.string   "sss",        limit: 20
    t.string   "time_zone",  limit: 50
    t.string   "status",     limit: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "dependents", force: :cascade do |t|
    t.integer  "employee_id",  limit: 4
    t.string   "name",         limit: 100
    t.string   "relationship", limit: 50
    t.date     "birthday"
    t.string   "status",       limit: 1
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "dependents", ["employee_id"], name: "index_dependents_on_employee_id", using: :btree

  create_table "emergency_contacts", force: :cascade do |t|
    t.integer  "employee_id",  limit: 4
    t.string   "name",         limit: 100
    t.string   "relationship", limit: 50
    t.string   "home_no",      limit: 15
    t.string   "mobile_no",    limit: 25
    t.string   "office_no",    limit: 25
    t.string   "status",       limit: 1
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "emergency_contacts", ["employee_id"], name: "index_emergency_contacts_on_employee_id", using: :btree

  create_table "employees", force: :cascade do |t|
    t.date     "date_employed"
    t.date     "date_regularized"
    t.date     "date_separated"
    t.string   "last_name",              limit: 100
    t.string   "first_name",             limit: 100
    t.string   "middle_name",            limit: 100
    t.integer  "business_unit_id",       limit: 4
    t.integer  "job_title_id",           limit: 4
    t.integer  "job_level_id",           limit: 4
    t.integer  "job_status_id",          limit: 4
    t.string   "biometrics_id",          limit: 10
    t.string   "falco_id",               limit: 10
    t.string   "gender",                 limit: 1
    t.string   "street",                 limit: 100
    t.string   "city",                   limit: 50
    t.string   "province",               limit: 50
    t.string   "zip",                    limit: 20
    t.string   "home_no",                limit: 15
    t.string   "mobile_no",              limit: 25
    t.string   "email",                  limit: 50
    t.string   "email2",                 limit: 50
    t.date     "birthday"
    t.time     "time_in"
    t.time     "time_out"
    t.string   "role",                   limit: 15
    t.string   "is_admin",               limit: 1
    t.string   "tin",                    limit: 20
    t.string   "pagibig",                limit: 20
    t.string   "philhealth",             limit: 20
    t.string   "sss",                    limit: 20
    t.float    "vacation_leave_balance", limit: 24
    t.float    "sick_leave_balance",     limit: 24
    t.string   "status",                 limit: 1
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "employee_no",            limit: 20
    t.float    "vacation_leave_credit",  limit: 24
    t.float    "sick_leave_credit",      limit: 24
  end

  add_index "employees", ["business_unit_id"], name: "index_employees_on_business_unit_id", using: :btree
  add_index "employees", ["job_level_id"], name: "index_employees_on_job_level_id", using: :btree
  add_index "employees", ["job_status_id"], name: "index_employees_on_job_status_id", using: :btree
  add_index "employees", ["job_title_id"], name: "index_employees_on_job_title_id", using: :btree

  create_table "equipment_request_items", force: :cascade do |t|
    t.integer  "equipment_request_id", limit: 4
    t.string   "item",                 limit: 100
    t.float    "quantity",             limit: 24
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "unit",                 limit: 30
  end

  add_index "equipment_request_items", ["equipment_request_id"], name: "index_equipment_request_items_on_equipment_request_id", using: :btree

  create_table "equipment_requests", force: :cascade do |t|
    t.integer  "employee_id",          limit: 4
    t.date     "target_delivery_date"
    t.string   "purpose",              limit: 250
    t.string   "status",               limit: 1
    t.string   "confirmation_status",  limit: 1
    t.string   "remark",               limit: 100
    t.integer  "reviewed_by",          limit: 4
    t.integer  "confirmed_by",         limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "equipment_requests", ["confirmed_by"], name: "confirmed_by", using: :btree
  add_index "equipment_requests", ["employee_id"], name: "index_equipment_requests_on_employee_id", using: :btree
  add_index "equipment_requests", ["reviewed_by"], name: "reviewed_by", using: :btree

  create_table "job_levels", force: :cascade do |t|
    t.string   "level",      limit: 4
    t.string   "rank",       limit: 30
    t.string   "grade",      limit: 2
    t.string   "status",     limit: 1
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "job_statuses", force: :cascade do |t|
    t.string   "name",       limit: 50
    t.string   "status",     limit: 1
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "job_titles", force: :cascade do |t|
    t.string   "name",        limit: 50
    t.string   "description", limit: 200
    t.string   "status",      limit: 1
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "leaves", force: :cascade do |t|
    t.integer  "employee_id",  limit: 4
    t.date     "start_date"
    t.date     "end_date"
    t.float    "no_of_days",   limit: 24
    t.string   "leave_type",   limit: 30
    t.string   "reason",       limit: 250
    t.string   "status",       limit: 1
    t.string   "remark",       limit: 100
    t.integer  "reviewed_by",  limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.float    "credits_used", limit: 24
  end

  add_index "leaves", ["employee_id"], name: "index_leaves_on_employee_id", using: :btree
  add_index "leaves", ["reviewed_by"], name: "reviewed_by", using: :btree

  create_table "official_businesses", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "purpose",     limit: 250
    t.string   "remark",      limit: 100
    t.string   "status",      limit: 1
    t.integer  "reviewed_by", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "client",      limit: 50
    t.integer  "employee_id", limit: 4
    t.integer  "ob_group_id", limit: 4
  end

  add_index "official_businesses", ["employee_id"], name: "employee_id", using: :btree
  add_index "official_businesses", ["ob_group_id"], name: "index_official_businesses_on_ob_group_id", using: :btree
  add_index "official_businesses", ["reviewed_by"], name: "reviewed_by", using: :btree

  create_table "offset_overtimes", force: :cascade do |t|
    t.integer  "overtime_id", limit: 4
    t.integer  "offset_id",   limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "hours_used",  limit: 4
  end

  add_index "offset_overtimes", ["offset_id"], name: "index_offset_overtimes_on_offset_id", using: :btree
  add_index "offset_overtimes", ["overtime_id"], name: "index_offset_overtimes_on_overtime_id", using: :btree

  create_table "offsets", force: :cascade do |t|
    t.date     "date"
    t.integer  "employee_id", limit: 4
    t.string   "offset_type", limit: 30
    t.string   "purpose",     limit: 250
    t.integer  "no_of_hours", limit: 4
    t.string   "remark",      limit: 100
    t.string   "status",      limit: 1
    t.integer  "reviewed_by", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "offsets", ["employee_id"], name: "employee_id", using: :btree
  add_index "offsets", ["reviewed_by"], name: "reviewed_by", using: :btree

  create_table "overtimes", force: :cascade do |t|
    t.integer  "employee_id",             limit: 4
    t.datetime "start_date"
    t.datetime "end_date"
    t.float    "no_of_hours",             limit: 24
    t.integer  "calendar_classification", limit: 1
    t.string   "expected_output",         limit: 250
    t.string   "status",                  limit: 1
    t.string   "remark",                  limit: 100
    t.integer  "reviewed_by",             limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "overtimes", ["employee_id"], name: "index_overtimes_on_employee_id", using: :btree
  add_index "overtimes", ["reviewed_by"], name: "reviewed_by", using: :btree

  create_table "special_days", force: :cascade do |t|
    t.date     "date"
    t.string   "name",       limit: 100
    t.string   "day_type",   limit: 50
    t.string   "status",     limit: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "undertimes", force: :cascade do |t|
    t.integer  "employee_id", limit: 4
    t.datetime "date"
    t.string   "nature",      limit: 20
    t.string   "reason",      limit: 250
    t.string   "status",      limit: 1
    t.string   "remark",      limit: 100
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "reviewed_by", limit: 4
  end

  add_index "undertimes", ["employee_id"], name: "index_undertimes_on_employee_id", using: :btree
  add_index "undertimes", ["reviewed_by"], name: "fk_reviewed_by", using: :btree

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "uid",              limit: 100
    t.string   "name",             limit: 100
    t.string   "oauth_token",      limit: 100
    t.datetime "oauth_expires_at"
    t.string   "provider",         limit: 100
    t.string   "email",            limit: 100
  end

  add_foreign_key "approvers", "employees", column: "approver_id", name: "fk_approver_id"
  add_foreign_key "equipment_requests", "employees", column: "confirmed_by", name: "equipment_requests_ibfk_1"
  add_foreign_key "equipment_requests", "employees", column: "reviewed_by", name: "equipment_requests_ibfk_2"
  add_foreign_key "leaves", "employees", column: "reviewed_by", name: "leaves_ibfk_1"
  add_foreign_key "official_businesses", "employees", column: "reviewed_by", name: "official_businesses_ibfk_1"
  add_foreign_key "official_businesses", "employees", name: "official_businesses_ibfk_2"
  add_foreign_key "offsets", "employees", column: "reviewed_by", name: "offsets_ibfk_1"
  add_foreign_key "offsets", "employees", name: "offsets_ibfk_2"
  add_foreign_key "overtimes", "employees", column: "reviewed_by", name: "overtimes_ibfk_1"
  add_foreign_key "undertimes", "employees", column: "reviewed_by", name: "fk_reviewed_by"
end
